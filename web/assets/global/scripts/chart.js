var Charts = function () {
    var chart = AmCharts.makeChart("chart_container", {
        "type": "serial",
        "theme": "light",
        "pathToImages": Metronic.getGlobalPluginsPath() + "amcharts/amcharts/images/",
        "autoMargins": false,
        "marginLeft": 60,
        "marginRight": 8,
        "marginTop": 10,
        "marginBottom": 26,

        "fontFamily": 'Open Sans',
        "color": '#888',

        "dataProvider": reportData,
        "valueAxes": [{
            "axisAlpha": 0,
            "position": "left",
            "width": 100
        }],
        "startDuration": 1,
        "graphs": [{
            "alphaField": "alpha",
            "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>",
            "dashLengthField": "dashLengthColumn",
            "fillAlphas": 1,
            "title": "Deposit",
            "type": "column",
            "valueField": "DEPOSIT_AMOUNT"
        }],
        "categoryField": "TRANSACTION_YEAR",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "tickLength": 0
        }
    });

    $('#chart_container').closest('.portlet').find('.fullscreen').click(function () {
        chart.invalidateSize();
    });

};