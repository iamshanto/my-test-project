var ReportCommon = function($){

    function monthlyReportData(reportDataUrl) {
        $('.show-report').click(function(){
            var el = $(".report-body");
            var data = $('form.report-form').serialize();

            $.ajax({
                url: reportDataUrl,
                dataType: 'json',
                data: data,
                success: function(json)
                {
                    Metronic.unblockUI(el);
                    $('.report-body').html(json.html);
                    if ($('.report-body table.report-table tbody tr').length) {
                        ReportChar.init($('#bar-chart'), json.chartData)
                    } else {
                        $('#bar-chart').remove();
                    }
                },
                beforeSend: function(){
                    Metronic.blockUI({
                        target: el,
                        animate: true,
                        overlayColor: 'black'
                    });
                },
                error: function(){
                    Metronic.unblockUI(el);
                }
            });
        });
    }

    function liveReportData(reportDataUrl) {
        function getReport(){
            var el = $(".report-body");

            $.ajax({
                url: reportDataUrl,
                success: function(html)
                {
                    Metronic.unblockUI(el);
                    $('.report-body').html(html);
                },
                beforeSend: function(){
                    Metronic.blockUI({
                        target: el,
                        animate: true,
                        overlayColor: 'black'
                    });
                },
                error: function(){
                    Metronic.unblockUI(el);
                }
            });
        }

        $('.reload-live-report').click(getReport).trigger('click');
    }

    function auditReportData(reportDataUrl) {
        $('.show-report').click(function(){
            var el = $(".report-body");
            var user = $('[name=user]').val();
            var date = $('[name=date]').val();

            $.ajax({
                url: reportDataUrl,
                data: 'user=' + user + '&date=' + date,
                success: function(html)
                {
                    Metronic.unblockUI(el);
                    $('.report-body').html(html);
                },
                beforeSend: function(){
                    Metronic.blockUI({
                        target: el,
                        animate: true,
                        overlayColor: 'black'
                    });
                },
                error: function(){
                    Metronic.unblockUI(el);
                }
            });
        }).trigger('click');
    }

    function kioskReportData(reportDataUrl) {
        $('.show-report').click(function(){
            var el = $(".report-body");
            var data = $('#kiosk-filter').serialize();

            $.ajax({
                url: reportDataUrl,
                data: data,
                success: function(html)
                {
                    Metronic.unblockUI(el);
                    $('.report-body').html(html);
                },
                beforeSend: function(){
                    Metronic.blockUI({
                        target: el,
                        animate: true,
                        overlayColor: 'black'
                    });
                },
                error: function(){
                    Metronic.unblockUI(el);
                }
            });
        }).trigger('click');
    }

    return {
        monthlyReportData : monthlyReportData,
        liveReportData : liveReportData,
        auditReportData : auditReportData,
        kioskReportData : kioskReportData
    };
}(jQuery);