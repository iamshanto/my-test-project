var ReportKiosk = function ($) {

    var $table = $("#kiosk-status-table");
    var $url;

    function checkStatus(callback) {
        $.get($url, function (response) {
            callback(response);
        });
    }

    function updateStatusClass(response) {
        for (var item in response) {
            var el = $table.find('tr.kiosk-' + item);

            el.removeClass('danger')
                .removeClass('success')
                .addClass(response[item]['status'] ? 'success' : 'danger');
        }
    }

    function refreshTable() {
        checkStatus(function (response) {
            updateStatusClass(response);
            setTimeout(refreshTable, 15 * 1000);
        });
    }

    function init(url) {
        $url = url;
        refreshTable();
    }

    return {
        init: init
    };

}(jQuery);