var DailyReport = function($){

    function init(reportDataUrl) {
        $('.show-report').click(function(){
            var el = $(".report-body");
            var kioskCode = $('[name=kioskCode]').val();
            var date = $('[name=date]').val();

            if (!kioskCode || !date) {
                bootbox.alert("Kiosk Code and Date Require");
                return false;
            }

            $.ajax({
                url: reportDataUrl,
                dataType: 'json',
                data: 'kioskCode=' + kioskCode + '&date=' + date,
                success: function(json)
                {
                    Metronic.unblockUI(el);
                    $('.report-body').html(json.html);
                    if ($('.report-body table.report-table tbody tr').length) {
                        ReportChar.init($('#bar-chart'), json.chartData)
                    } else {
                        $('#bar-chart').remove();
                    }
                },
                beforeSend: function(){
                    Metronic.blockUI({
                        target: el,
                        animate: true,
                        overlayColor: 'black'
                    });
                },
                error: function(){
                    Metronic.unblockUI(el);
                }
            });
        });
    }

    return {
        init : init
    };
}(jQuery);