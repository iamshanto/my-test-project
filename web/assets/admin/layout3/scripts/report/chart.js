var ReportChar = function($){

    function barChar(elm, data) {

        var options = {
            series: {
                bars: {
                    show: true
                }
            },
            bars: {
                barWidth: 0.8,
                lineWidth: 0, // in pixels
                shadowSize: 0,
                align: 'center'
            },

            grid: {
                tickColor: "#eee",
                borderColor: "#eee",
                borderWidth: 1
            },
            xaxis: {
                mode: "categories",
                tickLength: 0
            }
        };

        console.log(data);
        if (elm.length !== 0) {
            $.plot(elm, [{
                data: data,
                lines: {
                    lineWidth: 1
                },
                shadowSize: 0
            }], options);
        }

    }
    function init(elm, data) {
        barChar(elm, data);

        if ('matchMedia' in window) {
            // Chrome, Firefox, and IE 10 support mediaMatch listeners
            window.matchMedia('print').addListener(function(media) {
                barChar(elm, data);
            });
        } else {
            // IE and Firefox fire before/after events
            window.onbeforeprint = function () {
                barChar(elm, data);
            }
        }
    }

    return {
        init : init
    };
}(jQuery);