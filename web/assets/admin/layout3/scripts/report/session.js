var SessionReport = function (sessionIdRetrieveUrl, reportDataUrl) {
    $ = jQuery;

    $('[name=kiosk_name], [name=date]').change(function(e){

        var el = $(".filter-option-body");
        var kioskCode = $('[name=kiosk_name]').val();
        var date = $('[name=date]').val();
        var sessionElm = $('[name=session_id]');

        if (!kioskCode || !date) {
            sessionElm.find('option').remove();
            return false;
        }

        sessionElm.find('option').remove();

        $.ajax({
            url: sessionIdRetrieveUrl,
            data: 'kioskCode=' + kioskCode + '&date=' + date,
            success: function(json)
            {
                Metronic.unblockUI(el);
                for (i = 0; i < json.length; i++) {
                    var sessionId = json[i].SESSIONID;
                    sessionElm.append('<option>'+sessionId+'</option>');
                    if (i == 0) {
                        sessionElm.val(sessionId);
                    }
                }
            },
            error: function(){
                Metronic.unblockUI(el);
            },
            beforeSend: function(){
                Metronic.blockUI({
                    target: el,
                    animate: true,
                    overlayColor: 'black'
                });
            }
        });
    });
    $('[name=date]').change();

    $('.show-report').click(function(){
        var el = $(".report-body");
        var sessionElm = $('[name=session_id]').val();
        var kioskCode = $('[name=kiosk_name]').val();

        if (!sessionElm) {
            bootbox.alert("Session ID Require");
            return false;
        }

        $.ajax({
            url: reportDataUrl,
            data: 'sessionId=' + sessionElm + '&kioskCode=' + kioskCode,
            success: function(html)
            {
                Metronic.unblockUI(el);
                $('.report-body').html(html);
            },
            beforeSend: function(){
                Metronic.blockUI({
                    target: el,
                    animate: true,
                    overlayColor: 'none'
                });
            },
            error: function(){
                Metronic.unblockUI(el);
            }
        });
    });

};