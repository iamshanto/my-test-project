var App = function($){

    function init()
    {
        $('.delete_confirm').click(function(){
            var url = $(this).attr('href');
            bootbox.confirm("Are you sure?", function(result) {
                if (result) {
                    document.location.href = url;
                }
            });

            return false;
        });

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
        }

        handlePrint();
    }

    function dateRangeHandler(el, format) {
        if (typeof(format) == 'undefined') {
            format = 'YYYY-MM-DD';
        }
        el.daterangepicker({
                opens: (Metronic.isRTL() ? 'left' : 'right'),
                format: format,
                separator: ' to ',
                startDate: moment(),
                endDate: moment(),
                readOnly: true
            },
            function (start, end) {
                el.find('input').val(start.format(format) + ' to ' + end.format(format));
            }
        );
    }

    function handlePrint()
    {
        $('.print-report').click(function(){
            console.log()
            if ($('.report-body table.report-table tbody tr').length) {
                window.print();
            }
        });
    }

    return {
        init : init,
        dateRangeHandler: dateRangeHandler
    };
}(jQuery);