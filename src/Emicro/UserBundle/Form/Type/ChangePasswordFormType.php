<?php
namespace Emicro\UserBundle\Form\Type;

use AppBundle\Repository\SettingRepository;
use Emicro\UserBundle\Permission\Provider\SecurityPermissionProvider;
use Emicro\UserBundle\Validator\Constraints\Alpha;
use Emicro\UserBundle\Validator\Constraints\Alphanumeric;
use Emicro\UserBundle\Validator\Constraints\CustodianPassword;
use Emicro\UserBundle\Validator\Constraints\Digit;
use Emicro\UserBundle\Validator\Constraints\IsNewPassword;
use Emicro\UserBundle\Validator\Constraints\LowerCase;
use Emicro\UserBundle\Validator\Constraints\UpperCase;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\Length;

class ChangePasswordFormType extends AbstractType
{
    private $class;

    /**
     * @param string $class The User class name
     */

    private $securityContext;
    private $settingRepository;
    public function __construct($class, SecurityContext $securityContext, SettingRepository $settingRepository)
    {
        $this->class = $class;
        $this->securityContext = $securityContext;
        $this->settingRepository = $settingRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('current_password', 'password', array(
            'label' => 'form.current_password',
            'translation_domain' => 'FOSUserBundle',
            'mapped' => false,
            'constraints' => new UserPassword(),
        ));
        $builder->add('plainPassword', 'repeated', array(
            'type' => 'password',
            'options' => array('translation_domain' => 'FOSUserBundle'),
            'first_options' => array('label' => 'form.new_password'),
            'second_options' => array('label' => 'form.new_password_confirmation'),
            'invalid_message' => 'fos_user.password.mismatch',
            'constraints' => $this->getPasswordConstrains()
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'intention'  => 'change_password',
        ));
    }

    public function getName()
    {
        return 'emicro_user_change_password';
    }

    private function getPasswordConstrains()
    {
        $setting = $this->settingRepository->getSettings();
        $minLength = (isset($setting['min_password_length'])) ? (int)$setting['min_password_length'] : 6;
        $constrains[] = new Length(array(
            'min' => $minLength
        ));
        $constrains[] = new IsNewPassword($this->securityContext->getToken()->getUser());
        $constrains[] = new Digit();

        $user = $this->securityContext->getToken()->getUser();
        if ($user && in_array(SecurityPermissionProvider::ROLE_CUSTODIAN, $user->getRoles())) {
            $constrains[] = new CustodianPassword();
        } else {
            $constrains[] = new UpperCase();
            $constrains[] = new LowerCase();
        }

        return $constrains;
    }
}
