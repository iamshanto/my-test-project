<?php

namespace Emicro\UserBundle\Manager;

use AppBundle\Repository\SettingRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Emicro\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\CanonicalizerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class UserManager extends \FOS\UserBundle\Doctrine\UserManager
{
    /** @var  SettingRepository */
    protected $settingRepo;

    public function __construct(EncoderFactoryInterface $encoderFactory, CanonicalizerInterface $usernameCanonicalizer, CanonicalizerInterface $emailCanonicalizer, ObjectManager $om, $class)
    {
        parent::__construct($encoderFactory, $usernameCanonicalizer, $emailCanonicalizer, $om, $class);
    }

    public function setSettingsRepo(SettingRepository $settingRepository)
    {
        $this->settingRepo = $settingRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function findUserBy(array $criteria)
    {
        $user = $this->repository->findOneBy($criteria);

        /** @var User $user */
        if (null != $user) {
            $user->setPasswordChangeFrequency((int)$this->settingRepo->getSettingByName('password_change_frequency_in_days', 30));
        }

        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function findUsers()
    {
        return $this->repository->findAll();
    }

    /**
     * Updates a user.
     *
     * @param UserInterface $user
     * @param Boolean $andFlush Whether to flush the changes (default true)
     */
    public function updateUser(UserInterface $user, $andFlush = true)
    {
        $this->updateCanonicalFields($user);
        $this->updatePassword($user);

        $this->repository->save($user);
        return $user;
    }

    public function getDelta($old, $new)
    {
        return $this->repository->getDelta($old, $new);
    }

    /**
     * {@inheritDoc}
     */
    public function updatePassword(UserInterface $user)
    {
        /** @var User $user */
        if (0 !== strlen($password = $user->getPlainPassword())) {
            $encoder = $this->getEncoder($user);
            $user->setPassword($encoder->encodePassword($password, $user->getSalt()));
            $user->setPasswordChangedAt(new \DateTime('now'));
            $user->setForcePasswordChange(false);
            $user->eraseCredentials();
        }
    }

    public function isPasswordValid($password, UserInterface $user)
    {
        $encodedPass = $this->getEncoder($user)->encodePassword($password, $user->getSalt());

        return $encodedPass == $user->getPassword();
    }

    public function deleteUser(UserInterface $user)
    {
        $this->repository->delete($user);
    }
}
