<?php

/*
 * This file is part of the Docudex project.
 *
 * (c) Mohammad Emran Hasan <phpfour@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Emicro\UserBundle\Permission\Provider;

/**
 * Security Provider
 *
 * @author Mohammad Emran Hasan <phpfour@gmail.com>
 */
class SecurityPermissionProvider implements ProviderInterface
{
    const ROLE_CUSTODIAN = 'ROLE_CUSTODIAN';

    public function getPermissions()
    {
        return array(
            'USER_MANAGE' => array(
                'ROLE_USER_LIST', 'ROLE_USER_ADD', 'ROLE_USER_EDIT', 'ROLE_USER_DELETE', 'ROLE_USER_LOCK', 'ROLE_USER_UNLOCK',
                'ROLE_USER_GROUP_LIST', 'ROLE_USER_GROUP_ADD', 'ROLE_USER_GROUP_EDIT', 'ROLE_USER_GROUP_DELETE',
            ),
            'CUSTODIAN' => array(self::ROLE_CUSTODIAN)
        );
    }
}