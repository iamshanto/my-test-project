<?php

namespace Emicro\UserBundle\Permission;

use Symfony\Component\Security\Core\Role\RoleHierarchy as BaseRoleHierarchy;

/**
 * Role Hierarchy
 *
 * @author Mohammad Emran Hasan <phpfour@gmail.com>
 */
class RoleHierarchy extends BaseRoleHierarchy
{
    public function __construct(array $hierarchy, PermissionBuilder $permissionBuilder)
    {
        parent::__construct($permissionBuilder->getPermissionHierarchy());
    }
} 