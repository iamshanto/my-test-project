<?php
namespace Emicro\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class Alpha extends Constraint
{
    public $message = 'Password must contain at least %min_length% alpha character.';
	public $min = 1;
}