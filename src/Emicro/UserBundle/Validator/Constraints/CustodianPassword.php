<?php
namespace Emicro\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class CustodianPassword extends Constraint
{
    public $message = 'Custodian User Password must contain in following Character A-D and 0-9';
}