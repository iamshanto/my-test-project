<?php
namespace Emicro\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CustodianPasswordValidator extends ConstraintValidator
{
    public function validate($password, Constraint $constraint)
    {
        $validPasswordChar = array('A','B','C','D','0','1','2','3','4','5','6','7','8','9');

        $chars = strlen($password);
        $num = 0;
        for ($i = 0; $i < $chars; ++$i) {
            if (!in_array(substr($password, $i, 1), $validPasswordChar)) {
                $num++;
            }
        }

        if ($num > 0) {
            $this->context->addViolation(
                $constraint->message
            );
        }
    }
}