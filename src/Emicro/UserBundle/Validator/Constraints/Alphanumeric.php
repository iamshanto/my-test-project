<?php
namespace Emicro\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class Alphanumeric extends Constraint
{
    public $message = 'Password must contain at least %min_length% alphanumeric (letter or number) character.';
	public $min = 1;
}