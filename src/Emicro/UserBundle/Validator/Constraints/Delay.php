<?php
namespace Emicro\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class Delay extends Constraint
{
    public $message = 'Password may only be changed in %time% from the last change.';
	public $delay_time = 3600;

    public function validatedBy()
    {
        return 'docudex_delay_password_validator';
    }
}