<?php
namespace Emicro\UserBundle\Validator\Constraints;

use Emicro\UserBundle\Entity\User;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DelayValidator extends ConstraintValidator
{
    /** @var SecurityContext $user */
    private $securityContext;

    /** @var User $user */
    private $user;

    public function __construct($securityContext)
    {
        $this->securityContext = $securityContext;
        $this->user = $this->securityContext->getToken()->getUser();
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$this->securityContext->isGranted('ROLE_USER')) {
            return true;
        }

        if (null === $passwordChangedAt = $this->user->getPasswordChangedAt()) {
            return true;
        }

        $diff = time() - $passwordChangedAt->getTimestamp();

        if ($diff < $constraint->delay_time) {
            $this->context->addViolation(
                $constraint->message,
                array('%time%' => $this->time_passed($constraint->delay_time))
            );
        }
    }

    /**
     * @param $timestamp
     * @Source : http://www.devnetwork.net/viewtopic.php?p=595063&sid=5c26cdaf9c77fc8a2f70246ca1ce9e23#p595063
     * @return string
     */
    public function time_passed($timestamp)
    {
        $diff = time() - (time() - (int)$timestamp);

        if ($diff == 0) {
            return ''; //just now
        }

        $intervals = array(
            1                   => array('year',    31556926),
            $diff < 31556926    => array('month',   2628000),
            $diff < 2629744     => array('week',    604800),
            $diff < 604800      => array('day',     86400),
            $diff < 86400       => array('hour',    3600),
            $diff < 3600        => array('minute',  60),
            $diff < 60          => array('second',  1)
        );

        $value = floor($diff / $intervals[1][1]);
        return $value . ' ' . $intervals[1][0] . ($value > 1 ? 's' : '');
    }
}