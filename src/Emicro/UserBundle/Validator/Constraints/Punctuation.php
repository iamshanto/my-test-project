<?php
namespace Emicro\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class Punctuation extends Constraint
{
    public $message = 'Password must contain at least %min_length% punctuation (not whitespace or an alphanumeric) character.';
	public $min = 1;
}