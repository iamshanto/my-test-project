<?php
namespace Emicro\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class Digit extends Constraint
{
    public $message = 'Password must contain at least %min_length% digit.';
	public $min = 1;
}