<?php
namespace Emicro\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class UpperCase extends Constraint
{
    public $message = 'Password must contain at least %min_length% uppercase character.';
	public $min = 1;
}