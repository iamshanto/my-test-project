<?php
namespace Emicro\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class PunctuationValidator extends ConstraintValidator
{
	public function validate($password, Constraint $constraint)
	{
		$chars = strlen($password);
		$num = 0;
		for ($i = 0; $i < $chars; ++$i) {
			if (ctype_punct(substr($password, $i, 1))) {
				$num++;
			}
		}

		if ($num < $constraint->min) {
			$this->context->addViolation(
				$constraint->message,
				array('%min_length%' => $constraint->min)
			);
		}
	}
}