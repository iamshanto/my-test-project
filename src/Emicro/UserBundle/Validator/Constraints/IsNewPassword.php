<?php
namespace Emicro\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class IsNewPassword extends Constraint
{
    public $message = 'Password must not match with last %max_password_store% passwords.';
    public $last_passwords = 10;
    public $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function validatedBy()
    {
        return 'docudex_old_password_validator';
    }
}