<?php
namespace Emicro\UserBundle\Validator\Constraints;

use AppBundle\Repository\SettingRepository;
use Emicro\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsNewPasswordValidator extends ConstraintValidator
{
    /** @var SecurityContext $user */
    private $securityContext;

    /** @var EncoderFactory $securityEncoder */
    private $securityEncoder;

    /** @var User $user */
    private $user;

    /** @var  SettingRepository */
    private $settingRepo;

    public function __construct($securityContext, $securityEncoder, $settingRepo)
    {
        $this->securityContext = $securityContext;
        $this->securityEncoder = $securityEncoder;
        $this->settingRepo = $settingRepo;
        $this->user = $this->securityContext->getToken()->getUser();
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$this->securityContext->isGranted('ROLE_USER')) {
            return true;
        }

        if ($constraint->user) {
            $this->user = $constraint->user;
        }
        $setting = $this->settingRepo->getSettings();
        $encoder = $this->securityEncoder->getEncoder($this->user);
        $password = $encoder->encodePassword($value, $this->user->getSalt());
        $maxLastPassword = (isset($setting['max_password_history'])) ? (int)$setting['max_password_history'] : 1;
        $oldPasswords = array_slice($this->user->getPasswordHistory(), -$maxLastPassword, $maxLastPassword);

        if (in_array($password, $oldPasswords)) {
			$this->context->addViolation(
				$constraint->message,
                array('%max_password_store%' => $maxLastPassword)
			);
		}
    }
}