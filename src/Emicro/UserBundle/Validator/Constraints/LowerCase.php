<?php
namespace Emicro\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class LowerCase extends Constraint
{
    public $message = 'Password must contain at least %min_length% lowercase character.';
	public $min = 1;
}