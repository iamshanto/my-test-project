<?php

namespace Emicro\UserBundle\Exception;

class BlankPasswordException extends BadPasswordException
{
    protected $message = 'Password should not be blank';
}