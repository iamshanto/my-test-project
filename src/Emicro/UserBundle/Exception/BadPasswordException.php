<?php

namespace Emicro\UserBundle\Exception;

class BadPasswordException extends \Exception
{
    protected $message = "Bad Password";
    protected $code = 400;
}