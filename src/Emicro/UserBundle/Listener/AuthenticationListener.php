<?php
namespace Emicro\UserBundle\Listener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class AuthenticationListener {

    /** @var  EntityManager */
    private $entityManager;

    /** @var \Emicro\UserBundle\Repository\UserRepository  */
    private $userRepository;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $this->entityManager->getRepository('EmicroUserBundle:User');
    }
    /**
     * onAuthenticationFailure
     *
     * @author 	Joe Sexton <joe@webtipblog.com>
     * @param 	AuthenticationFailureEvent $event
     */
    public function onAuthenticationFailure( AuthenticationFailureEvent $event )
    {
        $username = $event->getAuthenticationToken()->getUsername();
        if ($user = $this->getUser($username)) {
            $this->userRepository->handleLoginAttempt($user);
        }
    }

    /**
     * onAuthenticationSuccess
     *
     * @author 	Joe Sexton <joe@webtipblog.com>
     * @param 	InteractiveLoginEvent $event
     */
    public function onAuthenticationSuccess( InteractiveLoginEvent $event )
    {
        $user = $event->getAuthenticationToken()->getUser();
        $user->setLoginAttempt(0);
        $this->userRepository->save($user);
    }

    private function getUser($username)
    {
        return $this->userRepository->findOneByUsername($username);
    }

}