<?php
namespace Emicro\UserBundle\Listener;


use AppBundle\Repository\SettingRepository;
use Emicro\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\SecurityContextInterface;

class RequestListener {

    /** @var SessionInterface  */
    protected $session;

    /** @var SettingRepository  */
    protected $settingRepository;

    /** @var SecurityContextInterface  */
    protected $securityContext;

    /** @var RouterInterface */
    protected $router;

    public function __construct(SessionInterface $session, SecurityContextInterface $securityContext, RouterInterface $router, SettingRepository $settingRepository)
    {
        $this->session = $session;
        $this->settingRepository = $settingRepository;
        $this->securityContext = $securityContext;
        $this->router = $router;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST != $event->getRequestType()) {
            return;
        }

        $token = $this->securityContext->getToken();

        if (null === $token) {
            return null;
        }

        if (!$this->isUserAuthenticated() ) {
            return null;
        }

        if ($token instanceof PreAuthenticatedToken && $token->getProviderKey() == 'api') {
            return null;
        }

        $maxIdleTime = $this->getMaxIdleTime();

        /** @var User $user */
        $user = $token->getUser();
        if ($user->isForcePasswordChange() && $event->getRequest()->get('_route') !== 'fos_user_change_password') {
            $event->setResponse(new RedirectResponse($this->router->generate('fos_user_change_password')));
        } else if ($maxIdleTime > 0) {
            $lapse = time() - $this->session->getMetadataBag()->getLastUsed();

            if ($event->getRequest()->get('_route') !== 'fos_user_change_password' && $lapse > $maxIdleTime) {

                $this->securityContext->setToken(null);
                $this->session->invalidate();
                $this->session->getFlashBag()->set('error', 'You have been logged out due to inactivity.');
                // Change the route if you are not using FOSUserBundle.
                $event->setResponse(new RedirectResponse($this->router->generate('fos_user_security_login')));
            }
        }
    }

    private function getMaxIdleTime()
    {
        $setting = $this->settingRepository->getSettings();

        return (int)$setting['session_time_out'];
    }

    /**
     * @return bool
     */
    protected function isUserAuthenticated() {
        return $this->securityContext->isGranted( 'IS_AUTHENTICATED_FULLY' ) || $this->securityContext->isGranted( 'IS_AUTHENTICATED_REMEMBERED' );
    }
}