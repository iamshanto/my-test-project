<?php

namespace Emicro\UserBundle\Listener;

use AppBundle\Repository\SettingRepository;
use Emicro\UserBundle\Entity\User;
use Emicro\UserBundle\Manager\UserManager;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;

class ChangePasswordListener
{
	const MAX_OLD_PASSWORD_STORE = 10;
	/** @var  UserManager $userManager */
	private $userManager;

    /** @var  SettingRepository */
    private $settingRepository;

	public function __construct(UserManager $userManager, SettingRepository $settingRepository)
    {
	    $this->userManager = $userManager;
        $this->settingRepository = $settingRepository;
    }

	public function onChangePassword(FormEvent $event)
	{
        $this->updaterPassword($event->getForm()->getData());
	}

	public function onChangePasswordSuccess(FilterUserResponseEvent $event)
	{
        $this->updaterPassword($event->getUser());
	}

    public function onRegistrationCompleted(FilterUserResponseEvent $event)
    {
        $this->updaterPassword($event->getUser());
    }

	private function storeOldPassword($oldPasswords, $newPassword)
	{
        $setting = $this->settingRepository->getSettings();
        $oldPasswords = (array)$oldPasswords;
		if (count($oldPasswords) >= (int)$setting['max_password_history'] ) {
			unset($oldPasswords[0]);
		}

		array_push($oldPasswords, $newPassword);
		return array_unique($oldPasswords);
	}

    /**
     * @param User $user
     */
    public function updaterPassword($user)
    {
        $user->setPasswordHistory(
            $this->storeOldPassword(
                $user->getPasswordHistory(), $user->getPassword()
            )
        );
        $user->setForcePasswordChange(0);
        $this->userManager->updateUser($user);
    }
}