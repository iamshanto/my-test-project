<?php

namespace Emicro\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity(repositoryClass="Emicro\UserBundle\Repository\UserRepository")
 * @ORM\Table(name="SYSTEM_USERS")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="IID")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="NAME")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="integer", name="GROUP_IID")
     */
    private $groupId;

    /**
     * @var string
     *
     * @ORM\Column(type="integer", name="LOGIN_ATTEMPT")
     */
    private $loginAttempt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", name="LOCKED_RELEASE_AT")
     */
    private $lockReleaseAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", name="PASSWORD_CHANGED_AT")
     */
    private $passwordChangedAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="PASSWORD_HISTORY")
     */
    private $passwordHistory;

    /**
     * @var \boolean
     *
     * @ORM\Column(type="boolean", name="FORCE_PASSWORD_CHANGE")
     */
    private $forcePasswordChange;

    private $groupName;

    private $passwordChangeFrequency = 30;

    public function __construct()
    {
        parent::__construct();
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName ()
    {
        if (empty($this->name)) {
            return $this->username;
        }

        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName ($name)
    {
        $this->name = $name;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    public function getCredentialsExpireAt()
    {
        return $this->credentialsExpireAt;
    }

    /**
     * @return string
     */
    public function getGroupId ()
    {
        return $this->groupId;
    }

    /**
     * @param string $groupId
     */
    public function setGroupId ($groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @return mixed
     */
    public function getGroupName()
    {
        return $this->groupName;
    }

    /**
     * @return integer
     */
    public function getLoginAttempt ()
    {
        return $this->loginAttempt;
    }

    /**
     * @param integer $loginAttempt
     */
    public function setLoginAttempt ($loginAttempt)
    {
        $this->loginAttempt = $loginAttempt;
    }

    /**
     * @return \DateTime
     */
    public function getLockReleaseAt ()
    {
        return $this->lockReleaseAt;
    }

    /**
     * @param \DateTime $lockReleaseAt
     */
    public function setLockReleaseAt ($lockReleaseAt)
    {
        $this->lockReleaseAt = $lockReleaseAt;
    }

    public function isLocked()
    {
        return $this->locked || ($this->lockReleaseAt && $this->lockReleaseAt > new \DateTime());
    }

    public function getLockedStatus()
    {
        return $this->locked;
    }

    /**
     * @return string
     */
    public function getPasswordHistory()
    {
        return $this->passwordHistory ? $this->passwordHistory : array();
    }

    /**
     * @param string $passwordHistory
     */
    public function setPasswordHistory ($passwordHistory)
    {
        $this->passwordHistory = $passwordHistory;
    }

    /**
     * @return boolean
     */
    public function isForcePasswordChange()
    {
        return $this->forcePasswordChange || $this->isDurationPassedAfterPasswordChange($this->passwordChangeFrequency . " day");
    }

    public function getForcePasswordChange()
    {
        return $this->forcePasswordChange;
    }

    public function shouldWarnUserBefore($days)
    {
        return (!$this->forcePasswordChange && $this->isDurationPassedAfterPasswordChange(($this->passwordChangeFrequency - $days) . " day")) ? 'true' : 'false';
    }

    /**
     * @param boolean $forcePasswordChange
     */
    public function setForcePasswordChange($forcePasswordChange)
    {
        $this->forcePasswordChange = $forcePasswordChange;
    }

    /**
     * @param int $passwordChangeFrequency
     */
    public function setPasswordChangeFrequency($passwordChangeFrequency)
    {
        $this->passwordChangeFrequency = $passwordChangeFrequency;
    }

    /**
     * @param \DateTime $passwordChangedAt
     * @return User
     */
    public function setPasswordChangedAt($passwordChangedAt)
    {
        $this->passwordChangedAt = $passwordChangedAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPasswordChangedAt()
    {
        return $this->passwordChangedAt;
    }

    /**
     * @param $duration
     * @return bool
     */
    protected function isDurationPassedAfterPasswordChange($duration)
    {
        return ($this->passwordChangedAt && $this->passwordChangedAt < (new \DateTime())->modify("-" . $duration));
    }

    public function isAccountNonLocked()
    {
        return !$this->isLocked();
    }
}