<?php
namespace Emicro\UserBundle\Repository;

use AppBundle\Repository\EntityRepository;
use Emicro\UserBundle\Entity\User;
use Symfony\Component\PropertyAccess\PropertyAccess;

class UserRepository extends EntityRepository
{
    public function findOneById($id)
    {
        $row = $this->getEntityAsArray($id);
        if ($row) {
            return $this->prepareUserFromArray($row);
        }

        return null;
    }

    public function find($id)
    {
        return $this->findOneById($id);
    }

    public function findOneByUsername($username)
    {
        $sql = "SYSTEM_USERS_FIND_BY_USERNAME(:username, :results)";
        $connection = $this->getConnectionResource();

        $stmt = $this->prepareStoredProcedureStatement($sql);
        $results = oci_new_cursor($connection);
        oci_bind_by_name($stmt, ":username", $username, -1);
        oci_bind_by_name($stmt, ":results", $results, -1, OCI_B_CURSOR);
        oci_execute($stmt);
        oci_execute($results);

        while (($row = oci_fetch_array($results, OCI_ASSOC + OCI_RETURN_NULLS)) != false) {
            return $this->prepareUserFromArray($row);
        }

        return null;
    }

    public function findOneBy(array $criteria)
    {
        if (isset($criteria['id'])) {
            return $this->findOneById($criteria['id']);
        }

        if (isset($criteria['username'])) {
            return $this->findOneByUsername($criteria['username']);
        }

        if (isset($criteria['usernameCanonical'])) {
            return $this->findOneByUsername($criteria['usernameCanonical']);
        }

        return null;
    }

    public function save($entity)
    {
        $fields = $this->prepareUserFromObject($entity);

        $sql = sprintf(self::SAVE_SP_QUERY, $this->getTableName(), implode(", :", array_keys($fields)));
        $stmt = $this->prepareStoredProcedureStatement($sql);

        oci_bind_by_name($stmt, ":sp_out_status", $statusOut, 32);
        oci_bind_by_name($stmt, ":sp_out_message", $statusMsgOut, 500);

        $idField = null;
        foreach ($fields as $field => $value) {
            $length = ($field === 'IID') ? 32 : -1;
            oci_bind_by_name($stmt, ":$field", $fields[$field], $length);
        }
        oci_execute($stmt);

        if ($statusOut === '1') {
            $entity->setId($fields['IID']);
            return $entity;
        }

        return null;

    }

    public function prepareUserFromArray($userArray)
    {
        $user = new User();
        $user->setId($userArray['IID']);
        $user->setUsername($userArray['USERNAME']);
        $user->setUsernameCanonical($userArray['USERNAME_CANONICAL']);
        $user->setEmail($userArray['EMAIL']);
        $user->setEmailCanonical($userArray['EMAIL_CANONICAL']);
        $user->setEnabled(($userArray['ENABLED'] === '1') ? true : false);
        $user->setPassword($userArray['PASSWORD']);
        $user->setLocked(($userArray['LOCKED'] === '1') ? true : false);
        $user->setExpired(($userArray['EXPIRED'] === '1') ? true : false);
        $user->setCredentialsExpired(($userArray['CREDENTIALS_EXPIRED'] === '1') ? true : false);
        $user->setConfirmationToken($userArray['CONFIRMATION_TOKEN']);
        $user->setName($userArray['NAME']);
        $user->setRoles(unserialize($userArray['ROLES']));
        $user->setSalt($userArray['SALT']);
        $user->setGroupId($userArray['GROUP_IID']);
        $user->setLoginAttempt($userArray['LOGIN_ATTEMPT']);
        $user->setPasswordHistory(json_decode($userArray['PASSWORD_HISTORY'], true));
        $user->setForcePasswordChange(($userArray['FORCE_PASSWORD_CHANGE'] === '1') ? true : false);

        if (!empty($userArray['LOCKED_RELEASE_AT'])) {
            $user->setLockReleaseAt(new \DateTime($userArray['LOCKED_RELEASE_AT']));
        }

        if (!empty($userArray['PASSWORD_CHANGED_AT'])) {
            $user->setPasswordChangedAt(new \DateTime($userArray['PASSWORD_CHANGED_AT']));
        }

        if (!empty($userArray['LAST_LOGIN'])) {
            $user->setLastLogin(new \DateTime($userArray['LAST_LOGIN']));
        }
        if (!empty($userArray['EXPIRES_AT'])) {
            $user->setExpiresAt(new \DateTime($userArray['EXPIRES_AT']));
        }
        if (!empty($userArray['CREDENTIALS_EXPIRE_AT'])) {
            $user->setCredentialsExpireAt(new \DateTime($userArray['CREDENTIALS_EXPIRE_AT']));
        }
        if (!empty($userArray['PASSWORD_REQUESTED_AT'])) {
            $user->setLastLogin(new \DateTime($userArray['PASSWORD_REQUESTED_AT']));
        }

        return $user;
    }

    protected function prepareUserFromObject(User $user)
    {
        $userArray['IID'] = $user->getId();
        $userArray['USERNAME'] = $user->getUsername();
        $userArray['USERNAME_CANONICAL'] = $user->getUsernameCanonical();
        $userArray['EMAIL'] = $user->getEmail();
        $userArray['EMAIL_CANONICAL'] = $user->getEmailCanonical();
        $userArray['ENABLED'] = $user->isEnabled() ? 1 : 0;
        $userArray['SALT'] = $user->getSalt();
        $userArray['PASSWORD'] = $user->getPassword();
        $userArray['LAST_LOGIN'] = $user->getLastLogin() ? $user->getLastLogin()->format('Y-m-d H:i:s') : null;
        if ($user->getLockReleaseAt() && $user->getLockReleaseAt() > new \DateTime()) {
            $userArray['LOCKED'] = 0;
        } else {
            $userArray['LOCKED'] = $user->isLocked() ? 1 : 0;
        }
        $userArray['EXPIRED'] = $user->isExpired() ? 1 : 0;
        $userArray['EXPIRES_AT'] = $user->getExpiresAt() ? $user->getExpiresAt()->format('Y-m-d H:i:s') : null;
        $userArray['CONFIRMATION_TOKEN'] = $user->getConfirmationToken();
        $userArray['PASSWORD_REQUESTED_AT'] = $user->getPasswordRequestedAt() ? $user->getPasswordRequestedAt()->format('Y-m-d H:i:s') : null;
        $userArray['CREDENTIALS_EXPIRED'] = $user->isCredentialsExpired() ? 1 : 0;
        $userArray['CREDENTIALS_EXPIRE_AT'] = $user->getCredentialsExpireAt() ? $user->getCredentialsExpireAt()->format('Y-m-d H:i:s') : null;
        $userArray['NAME'] = $user->getName();
        $userArray['ROLES'] = serialize($user->getRoles());
        $userArray['GROUP_IID'] = $user->getGroupId();
        $userArray['IN_LOGIN_ATTEMPT'] = $user->getLoginAttempt();
        $userArray['LOCKED_RELEASE_AT'] = $user->getLockReleaseAt() ? $user->getLockReleaseAt()->format('Y-m-d H:i:s') : null;
        $userArray['PASSWORD_HISTORY'] = $user->getPasswordHistory() ? json_encode($user->getPasswordHistory()) : null;
        $userArray['FORCE_PASSWORD_CHANGE'] = !$user->getId() ? 1 : $user->getForcePasswordChange() ? 1 : 0;
        $userArray['PASSWORD_CHANGED_AT'] = $user->getPasswordChangedAt() ? $user->getPasswordChangedAt()->format('Y-m-d H:i:s') : null;

        return $userArray;
    }

    public function findAll()
    {
        $entityCollection = array();

        try {
            $connection = $this->getConnectionResource();

            $sql = sprintf(self::FIND_ALL_SP_QUERY, $this->getTableName());

            $stmt = $this->prepareStoredProcedureStatement($sql);

            $results = oci_new_cursor($connection);
            oci_bind_by_name($stmt, ":results", $results, -1, OCI_B_CURSOR);
            oci_execute($stmt);
            oci_execute($results);

            while (($row = oci_fetch_array($results, OCI_ASSOC + OCI_RETURN_NULLS)) != false) {
                $entityCollection[] = $this->prepareUserFromArray($row);
            }

        } catch (\Exception $e) {
            $entityCollection = parent::findAll();
        }

        return $entityCollection;
    }

    public function handleLoginAttempt(User $user)
    {
        $setting = $this->_em->getRepository('AppBundle:Setting')->getSettings();

        if ($user->getLoginAttempt() > (int)$setting['locked_on_login_attempt'] - 2) {
            $user->setLockReleaseAt(new \DateTime($setting['locked_out_duration']));
            $user->setLoginAttempt(0);
        } elseif (!$user->isLocked()) {
            $loginAttempt = $user->getLoginAttempt() ? $user->getLoginAttempt() + 1 : 1;
            $user->setLoginAttempt($loginAttempt);
        }

        $this->save($user);
    }

    public function getEntityValues($entity)
    {
        $validField = array('username', 'email', 'name', 'roles');
        $fields = $this->getClassMetadata()->getFieldNames();
        $accessor = PropertyAccess::createPropertyAccessor();
        $values = array();

        // /vendor/symfony/symfony/src/Symfony/Component/Form/FormRenderer.php
        foreach ($fields as $field) {
            if (in_array($field, $validField)) {
                $fieldName = ucfirst(trim(strtolower(preg_replace(array('/([A-Z])/', '/[_\s]+/'), array('_$1', ' '), $field))));
                $values[$fieldName] = $accessor->getValue($entity, $field);
            }
        }

        $values['Employee ID'] = $values['Username'];
        unset($values['Username']);

        return array_reverse($values);
    }
}