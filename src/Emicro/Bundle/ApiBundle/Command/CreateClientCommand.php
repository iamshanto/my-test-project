<?php

namespace Emicro\Bundle\ApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateClientCommand extends ContainerAwareCommand
{
    /** @var  \Emicro\Bundle\ApiBundle\Model\ClientManager */
    protected $clientManager;

    protected function configure()
    {
        $this
            ->setName('emicro:api:client:create')
            ->setDefinition(array(
                new InputArgument('name', InputArgument::REQUIRED, 'The Client Name'),
            ))
            ->setDescription('Create New Client');
    }

    /**
     * @see Symfony\Component\Console\Command\Command::initialize()
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->clientManager = $this->getContainer()->get('emicro_api.client_manager');
    }

    /**
     * @see Command
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument('name')) {

            $clientName = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Please choose a client name:',
                function ($clientName) {
                    if (empty($clientName)) {
                        throw new \Exception('Client name can not be empty');
                    }

                    return $clientName;
                }
            );

            $input->setArgument('name', $clientName);
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Creating Client.</info>');
        $output->writeln('');

        $client = $this->clientManager->create(array(
            'name'   => $input->getArgument('name')
        ));

        $output->writeln('<info>Client Created With Api Key : </info>');
        $output->writeln('');
        $output->writeln('<comment>' . $client->getApiKey() . '</comment>');
        $output->writeln('');
    }
}
