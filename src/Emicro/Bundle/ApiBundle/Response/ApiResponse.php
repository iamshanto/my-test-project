<?php

namespace Emicro\Bundle\ApiBundle\Response;

use JMS\Serializer\Annotation\XmlRoot;

/**
 * @XmlRoot("response")
 * */
class ApiResponse
{
    /**
     * @var int
     */
    private $status;

    private $message;
    private $content;

    public function __construct($message, $content, $status = 200)
    {
        $this->message = $message;
        $this->content = $content;
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
}