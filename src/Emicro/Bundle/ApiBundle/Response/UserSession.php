<?php

namespace Emicro\Bundle\ApiBundle\Response;

class UserSession
{
    protected $id;
    protected $name;
    protected $username;
    protected $email;
    protected $locked;
    protected $forcePasswordChange;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * @param mixed $locked
     * @return UserSession
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getForcePasswordChange()
    {
        return $this->forcePasswordChange;
    }

    /**
     * @param mixed $forcePasswordChange
     * @return $this
     */
    public function setForcePasswordChange($forcePasswordChange)
    {
        $this->forcePasswordChange = $forcePasswordChange;

        return $this;
    }
}