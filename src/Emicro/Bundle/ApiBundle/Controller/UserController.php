<?php

namespace Emicro\Bundle\ApiBundle\Controller;

use AppBundle\Event\FormValidationFailedEvent;
use AppBundle\Event\LoggableEvent;
use Emicro\Bundle\ApiBundle\Response\ApiResponse;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use JMS\SecurityExtraBundle\Annotation as JMS;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class UserController extends FOSRestController
{

    /**
     * Authenticate Custodian User,
     *
     * @ApiDoc(
     *   resource = false,
     *   description = "User login endpoint",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when  API-KEY Header missing",
     *     401 = "Returned when the credential failed",
     *     403 = "Returned when the user is locked/disabled"
     *   },
     *   requirements={
     *      {
     *          "name"="username",
     *          "dataType"="string",
     *          "description"="Username"
     *      },
     *      {
     *          "name"="password",
     *          "dataType"="string",
     *          "description"="User Password"
     *      }
     *   }
     * )
     *
     * @return array
     *
     * @throws NotFoundHttpException when page not exist
     * @throws BadCredentialsException when %api_header_key% not exist in header
     * @JMS\Secure(roles="ROLE_CUSTODIAN")
     */
    public function loginUserAction()
    {
        $data = $this->get('emicro_api.user.handler')->login($this->getUser());

        if($data->getStatus() == 200) {
            $description = sprintf("User '%s' Authenticated Successfully through api", $this->getUser()->getName());
            $this->get('event_dispatcher')->dispatch(
                'security.api.authenticated',
                new LoggableEvent('User Logged in', $description)
            );
        }

        return $this->handleView($this->view($data, $data->getStatus()));
    }

    /**
     * Change custodian user password,
     *
     * @ApiDoc(
     *   resource = false,
     *   description = "Change User Password",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when  API-KEY Header missing/Validation failed",
     *     401 = "Returned when the credential failed",
     *     403 = "Returned when the user is locked/disabled"
     *   },
     *   requirements={
     *      {
     *          "name"="username",
     *          "dataType"="string",
     *          "description"="Username"
     *      },
     *      {
     *          "name"="password",
     *          "dataType"="string",
     *          "description"="User Old Password"
     *      },
     *      {
     *          "name"="new_password",
     *          "dataType"="string",
     *          "description"="New Password"
     *      }
     *   }
     * )
     *
     * @param Request $request
     * @return array
     * @JMS\Secure(roles="ROLE_CUSTODIAN")
     */
    public function changePasswordAction(Request $request)
    {
        $user = $this->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $form = $this->createChangePasswordForm($request->request, $user);

        if ($form->isValid()) {

            $data = $this->get('emicro_api.user.handler')->changePassword(
                $user,
                $request->request->get('new_password')
            );

            $response = $this->handleView($this->view($data, $data->getStatus()));

            $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        }

        $dispatcher->dispatch('edit.validation.failed', new FormValidationFailedEvent($form));

        return $this->handleView($this->view($this->createErrorResponse($form->getErrors(false, true)), 400));
    }

    /**
     * @param ParameterBag $request
     * @param $user
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createChangePasswordForm(ParameterBag $request, $user)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.change_password.form.factory');

        $form = $formFactory->createForm();

        $parameters = array(
            'current_password' => $request->get('password'),
            '_token' => $this->get('security.csrf.token_manager')->getToken($form->getConfig()->getOption('intention')),
            'plainPassword' => array(
                'first' => $request->get('new_password'),
                'second' => $request->get('new_password')
            ),
        );

        $form->setData($user);
        $form->submit($parameters);

        return $form;
    }

    /**
     * @param $errors
     * @return ApiResponse
     */
    protected function createErrorResponse($errors)
    {
        return new ApiResponse('Some form validation error occurred!', $errors, 400);
    }
}
