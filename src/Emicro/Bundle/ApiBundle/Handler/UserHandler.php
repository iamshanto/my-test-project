<?php

namespace Emicro\Bundle\ApiBundle\Handler;

use Emicro\Bundle\ApiBundle\Response\ApiResponse;
use Emicro\Bundle\ApiBundle\Response\UserSession;
use Emicro\UserBundle\Entity\User;
use Emicro\UserBundle\Manager\UserManager;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserHandler
{
    /**
     * @var UserManager
     */
    private $manager;

    public function __construct(UserManager $manager)
    {
        $this->manager = $manager;
    }

    public function login(User $user)
    {
        if (null == $user) {
            throw new HttpException(401, 'Username not provided');
        }

        $session = new UserSession();

        $session
            ->setId($user->getId())
            ->setName($user->getName())
            ->setUsername($user->getUsername())
            ->setEmail($user->getEmail())
            ->setForcePasswordChange($user->isForcePasswordChange())
            ->setLocked($user->isLocked());

        if ($user->isLocked() || !$user->isEnabled()) {
            $msg = 'Account is locked/disabled try again after a while';
            $status = 403;
        } else {
            $msg = 'Authenticated';
            $status = 200;
        }

        return new ApiResponse($msg, $session, $status);
    }

    public function changePassword(User $user, $newPassword)
    {
        if ($user->isLocked() || !$user->isEnabled()) {
            return new ApiResponse('Account is locked/disabled try again after a while', "failed", 403);
        }

        try {
            $user->setPlainPassword($newPassword);
            $this->manager->updateUser($user);
            return new ApiResponse("Password changed!", "success", 200);

        } catch (\Exception $e) {
            throw new HttpException(400, $e->getMessage());
        }
    }
}