<?php


namespace Emicro\Bundle\ApiBundle\Security;

use Emicro\Bundle\ApiBundle\Model\ClientManager;
use Emicro\UserBundle\Manager\UserManager;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class ApiKeyAuthenticator implements SimplePreAuthenticatorInterface
{
    private $clientManager;
    /**
     * @var string
     */
    private $headerKey;
    /**
     * @var UserManager
     */
    private $manager;

    /** @var  TraceableEventDispatcher */
    private $dispatcher;

    public function __construct(ClientManager $clientManager, $headerKey = 'X-API-KEY')
    {
        $this->clientManager = $clientManager;
        $this->headerKey = $headerKey;
    }

    public function createToken(Request $request, $providerKey)
    {
        if (!$request->headers->has($this->headerKey)) {
            throw new HttpException(400, sprintf('%s header not found', $this->headerKey));
        }

        if ('' == $apikey = $request->headers->get($this->headerKey)) {
            throw new HttpException(400, sprintf('%s header must not be empty', $this->headerKey));
        }

        return new PreAuthenticatedToken(
            $request->headers->get($this->headerKey),
            array(
                'username' => $request->request->get('username'),
                'password' => $request->request->get('password')
            ),
            $providerKey
        );
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $this
            ->clientManager
            ->getClientFromApiKey($token->getUser());

        $user = $this->checkCredential($token);

        return new PreAuthenticatedToken(
            $user,
            $token->getUser(),
            $providerKey,
            $user->getRoles()
        );
    }

    protected function checkCredential(TokenInterface $token)
    {
        $credential = $token->getCredentials();

        $user = $this->manager->findUserBy(array('username' => $credential['username']));

        if ($user == null) {
            throw new HttpException(401, 'Invalid credential');
        }

        if (!$this->manager->isPasswordValid($credential['password'], $user)) {
            $token->setUser($user);
            $this->dispatcher->dispatch(
                AuthenticationEvents::AUTHENTICATION_FAILURE,
                new AuthenticationFailureEvent($token, new BadCredentialsException())
            );

            throw new HttpException(401, 'Invalid credential');
        }

        return $user;
    }


    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    /**
     * @param UserManager $manager
     * @return ApiKeyAuthenticator
     */
    public function setManager(UserManager $manager)
    {
        $this->manager = $manager;
        return $this;
    }

    /**
     * @param mixed $dispatcher
     */
    public function setDispatcher($dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }
}