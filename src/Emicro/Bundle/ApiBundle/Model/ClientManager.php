<?php

namespace Emicro\Bundle\ApiBundle\Model;

use Emicro\Bundle\ApiBundle\Entity\Client;
use Emicro\Bundle\ApiBundle\Repository\ClientRepository;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ClientManager
{
    /** @var  ClientRepository */
    private $clientRepository;

    public function __construct($clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function getClientFromApiKey($apiKey)
    {

        $client = $this->clientRepository->findByApiKey($apiKey);

        if (!$client) {
            throw new HttpException(401, 'API key not registered');
        }

        return $client;
    }

    public function changeKey(Client $client)
    {
        $client->setApiKey(md5(uniqid(rand(), TRUE)));
        return $this->clientRepository->save($client);
    }

    public function create($clientData)
    {
        $client = new Client();
        $client->setApiKey(md5(uniqid(rand(), TRUE)));
        $client->setName($clientData['name']);

        return $this->clientRepository->save($client);
    }
}
