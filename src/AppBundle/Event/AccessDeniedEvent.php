<?php

namespace AppBundle\Event;

use Symfony\Component\HttpFoundation\Request;

class AccessDeniedEvent extends BaseEvent
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getEventLogInfo($eventName)
    {
        return array(
            'description' => $this->getDescription(),
            'type' => 'Access Denied'
        );
    }

    private function getDescription()
    {
        return sprintf('Unauthorized access on url: "%s"', $this->request->getRequestUri());
    }
}