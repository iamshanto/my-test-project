<?php
namespace AppBundle\Event;

use Symfony\Component\PropertyAccess\PropertyAccess;

class EntityEvent extends BaseEvent
{
    protected $propertiesFound = array();

    /**
     * @var
     */
    protected $entity;
    /**
     * @var
     */
    protected $eventName;

    protected $changesValues = array();

    public function __construct($entity, $values)
    {
        $this->entity = $entity;
        $this->changesValues = $values;
    }

    /**
     * @param $eventName
     *
     * @return array
     */
    public function getEventLogInfo($eventName)
    {
        if (null == $this->changesValues) {
            return null;
        }

        $this->eventName = $eventName;
        $reflectionClass = $this->getReflectionClassFromObject($this->entity);

        $typeName = $this->getTypeName($reflectionClass);
        $eventType = $this->getEventType($typeName);
        $eventDescription = $this->getDescriptionString($reflectionClass, $typeName);

        return array(
            'description' => $eventDescription,
            'type' => $eventType,
        );

    }

    /**
     * @param string $typeName
     * @return string
     */
    protected function getEventType($typeName)
    {
        return $typeName . " " . $this->getEventShortName();
    }

    protected function getTypeName(\ReflectionClass $reflectionClass)
    {
        return $reflectionClass->getShortName();
    }

    protected function getDescriptionString(\ReflectionClass $reflectionClass, $typeName)
    {
        $property = $this->getIdentifierProperty($reflectionClass);

        $descriptionTemplate = '%s has been %s ';

        if ($property) {
            $descriptionTemplate .= sprintf(' %s = "%s" ', $property, $this->getProperty($property));
        }

        switch ($this->getEventShortName()){
            case 'updated':
            case 'deleted':
                $descriptionTemplate .= " old values : %s"; break;
            case 'created':
            default:
                $descriptionTemplate .= " with values : %s"; break;
        }

        return sprintf($descriptionTemplate,
            $typeName,
            $this->getEventShortName(),
            json_encode($this->changesValues)
        );
    }

    /**
     * @param string $name
     * @return string|mixed
     */
    protected function getProperty($name)
    {
        return PropertyAccess::createPropertyAccessor()->getValue($this->entity, $this->propertiesFound[$name]);
    }

    /**
     * @param \ReflectionClass $reflectionClass
     * @return null|string
     */
    protected function getIdentifierProperty(\ReflectionClass $reflectionClass)
    {
        $properties = $reflectionClass->getProperties(
            \ReflectionProperty::IS_PROTECTED | \ReflectionProperty::IS_PRIVATE
        );

        $propertyName = null;

        foreach ($properties as $property) {
            if ($this->isIdProperty(strtolower($property->name))) {
                $this->propertiesFound['id'] = $propertyName = $property->name;
            }
        }

        return $propertyName;
    }

    /**
     * @param string $property
     * @return bool
     */
    protected function isIdProperty($property)
    {
        return $property == 'id' || $property == 'iid';
    }

    /**
     * @return string
     */
    protected function getEventShortName()
    {
        return substr(strrchr($this->eventName, '.'), 1);
    }

    protected function getReflectionClassFromObject($object)
    {
        return new \ReflectionClass(get_class($object));
    }
}