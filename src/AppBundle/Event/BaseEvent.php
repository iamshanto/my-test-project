<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Xiidea\EasyAuditBundle\Resolver\EmbeddedEventResolverInterface;

abstract class BaseEvent extends Event implements EmbeddedEventResolverInterface
{

}