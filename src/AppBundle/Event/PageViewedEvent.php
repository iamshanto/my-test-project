<?php

namespace AppBundle\Event;


class PageViewedEvent extends LoggableEvent
{

    public function __construct($page)
    {
        parent::__construct('Page viewed', sprintf('"%s" page viewed', $page));
    }
}