<?php

namespace AppBundle\Event;


class LoggableEvent extends BaseEvent
{
    /**
     * @var
     */
    private $type;

    /**
     * @var
     */
    private $description;

    public function __construct($type, $description)
    {
        $this->type = $type;
        $this->description = $description;
    }

    public function getEventLogInfo($eventName)
    {
        return array(
            'description' => $this->description,
            'type' => $this->type
        );
    }
}