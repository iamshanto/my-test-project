<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Xiidea\EasyAuditBundle\Resolver\EmbeddedEventResolverInterface;

class ReportViewEvent extends Event implements EmbeddedEventResolverInterface
{
    /**
     * @var array
     */
    private $request;
    /**
     * @var
     */
    private $type;

    public function __construct($request = array(), $type)
    {
        $this->request = self::removeEmptyValue($request);
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function getRequest()
    {
        return $this->request;
    }

    public function getEventLogInfo($eventName)
    {
        return array(
            'description'=> $this->getDescription(),
            'type'=> $this->getType(),
        );
    }

    private function getDescription()
    {
        return "Filter Parameters: " . urldecode(http_build_query($this->getRequest()));
    }

    /**
     * @return string
     */
    protected function getType()
    {
        return 'Report:' . $this->type;
    }

    private static function removeEmptyValue($request)
    {
        $request = array_map(function ($v) {
            return is_array($v) ? array_filter($v) : $v;
        }, $request);

        return array_filter($request);
    }
}