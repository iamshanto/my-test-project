<?php

namespace AppBundle\Event;


use Symfony\Component\Form\Form;

class FormValidationFailedEvent extends BaseEvent
{
    /**
     * @var Form
     */
    private $form;

    public function __construct($form)
    {
        $this->form = $form;
    }

    public function getEventLogInfo($eventName)
    {
        return array(
            'description' => $this->getErrorAsString(),
            'type' => 'Validation Error'
        );
    }

    /**
     * @return string
     */
    protected function getErrorAsString()
    {
        return str_replace(PHP_EOL, ' ', (string)$this->form->getErrors(true, false));
    }
}