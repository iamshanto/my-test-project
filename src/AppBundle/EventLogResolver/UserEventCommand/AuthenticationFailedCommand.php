<?php

/*
 * This file is part of the XiideaEasyAuditBundle package.
 *
 * (c) Xiidea <http://www.xiidea.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace AppBundle\EventLogResolver\UserEventCommand;


use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Xiidea\EasyAuditBundle\Resolver\UserEventCommand\ResolverCommand;

class AuthenticationFailedCommand extends ResolverCommand
{
    /**
     * @param AuthenticationFailureEvent $event
     * @param array $default
     * @return array
     */
    public function resolve($event, $default = array())
    {
        if( !($event instanceof AuthenticationFailureEvent)) {
            return $default;
        }

        $requestOrigin = ($event->getAuthenticationToken()->getProviderKey() == 'api') ? "api" : "web";

        return $this->getEventDetailsArray(
            "Authentication Failed",
            "Bad credentials Username: '%s' from '{$requestOrigin}'",
            $event->getAuthenticationToken()->getUser()
        );
    }
}
