<?php
namespace AppBundle\Menu;


use Knp\Menu\FactoryInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttributes(array('class' => 'nav navbar-nav'));

        $menu->addChild('Dashboard', array('route' => 'homepage'));


        $menu->addChild('Entities', array('route' => 'homepage'))
            ->setAttribute('dropdown', true);

        $this
            ->addChildMenu($menu['Entities'], 'Bin Code', 'app_bin_code_list')
            ->addChildMenu($menu['Entities'], 'Back Office', 'app_back_office_list')
            ->addChildMenu($menu['Entities'], 'Kiosks', 'app_kiosk_list');

        $menu->addChild('Reports', array('route' => 'homepage'))
            ->setAttribute('dropdown', true);

        $this
            ->addChildMenu($menu['Reports'], 'Kiosk Status', 'app_kiosk_status_list', 'icon-bar-chart')
            ->addChildMenu($menu['Reports'], 'Live Transactions', 'app_transaction_report_live', 'icon-bar-chart')
            ->addChildMenu($menu['Reports'], 'Session Report', 'app_transaction_report_session', 'icon-bar-chart')
            ->addChildMenu($menu['Reports'], 'Daily Report', 'app_transaction_report_daily', 'icon-bar-chart')
            ->addChildMenu($menu['Reports'], 'Monthly Report', 'app_transaction_report_monthly', 'icon-bar-chart')
            ->addChildMenu($menu['Reports'], 'Yearly Report', 'app_transaction_report_yearly', 'icon-bar-chart')
            ->addChildMenu($menu['Reports'], 'Web Audit Log', 'app_audit_log', 'icon-bar-chart')
            ->addChildMenu($menu['Reports'], 'Kiosk Audit Log', 'app_audit_log_kiosk', 'icon-bar-chart')
        ;

        $menu->addChild('System Settings', array('route' => 'homepage'))
            ->setAttribute('dropdown', true);

        $this
            ->addChildMenu($menu['System Settings'], 'Users', 'app_user_list', 'icon-users')
            ->addChildMenu($menu['System Settings'], 'User Group', 'app_user_group_list', 'icon-users')
            ->addChildMenu($menu['System Settings'], 'Settings', 'app_settings_edit', 'icon-settings');

        $this->container->get('xiidea.easy_menu_acl.access_filter')->apply($menu);

        return $menu;
    }

    /**
     * @param $menu
     * @param $title
     * @param $route
     * @param $icon
     * @return mixed
     */
    protected function addChildMenu(MenuItem $menu, $title, $route, $icon = 'icon-briefcase')
    {
        $menu
            ->addChild($title, array('route' => $route))
            ->setAttribute('icon', $icon);

        return $this;
    }
}