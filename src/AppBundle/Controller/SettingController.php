<?php

namespace AppBundle\Controller;

use AppBundle\Event\LoggableEvent;
use AppBundle\Event\PageViewedEvent;
use AppBundle\Form\Type\SettingFormType;
use AppBundle\Repository\SettingRepository;
use JMS\SecurityExtraBundle\Annotation as JMS;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class SettingController extends BaseController
{

    /**
     * @Route("/settings", name="app_settings_edit")
     * @JMS\Secure(roles="ROLE_SETTING_EDIT")
     */
    public function settingUpdateAction(Request $request)
    {
        $settingsRepo = $this->getDoctrine()->getRepository('AppBundle:Setting');

        $formData = array('settings' => $settingsRepo->findAll());
        $form = $this->createForm(new SettingFormType(), $formData);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $delta = $this->getDelta($settingsRepo, $request->request->get($form->getName()));

                if (null !== $delta) {
                    $this->dispatch('setting.changed', $this->createSettingChangedEvent($delta['old']));
                    $settingsRepo->saveAll($delta['new']);
                }

                $this->get('session')->getFlashBag()
                    ->add('success', 'Settings Saved Successfully');

                return $this->redirect($this->generateUrl('app_settings_edit'));
            } else {
                $this->dispatch('page_viewed', new PageViewedEvent("Setting Update"));
            }
        } else {
            $this->dispatch('page_viewed', new PageViewedEvent("Settings page"));
        }

        return $this->render('Setting/add-edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    private function getDelta(SettingRepository $settingsRepo, $newData = array())
    {
        $delta = array(
            'old' => array_diff_assoc($settingsRepo->getSettings(), $newData),
            'new' => array_diff_assoc($newData, $settingsRepo->getSettings()),
        );

        return empty($delta['old']) ? null : $delta;
    }

    /**
     * @param $delta
     * @return LoggableEvent
     */
    protected function createSettingChangedEvent($delta)
    {
        return new LoggableEvent('Settings Changed', "old values: " . json_encode($delta));
    }

}
