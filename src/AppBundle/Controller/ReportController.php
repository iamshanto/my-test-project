<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use JMS\SecurityExtraBundle\Annotation as JMS;

class ReportController extends Controller
{
    /**
     * @Route("/report", name="app_report_list")
     */
    public function indexAction(Request $request)
    {
        /* TODO: Access Check */
    }

    /**
     * @Route("/report/yearly", name="app_report_yearly")
     */
    public function yearlyReportAction()
    {
        /* TODO: Access Check */
        $reportRepo = $this->get('app.report.repo');
        $data = $reportRepo->yearlyReportData();
        return $this->render('Report/index.html.twig', array(
            'data' => $data
        ));
    }

    /**
     * @Route("/report/monthly", name="app_report_monthly")
     */
    public function monthlyReportAction()
    {
        /* TODO: Access Check */
        $reportRepo = $this->get('app.report.repo');
        $data = $reportRepo->monthlyReportData();
        return $this->render('Report/index.html.twig', array(
            'data' => $data
        ));
    }

    /**
     * @Route("/report/daily", name="app_report_daily")
     */
    public function dailyReportAction()
    {
        /* TODO: Access Check */
        $reportRepo = $this->get('app.report.repo');
        $data = $reportRepo->dailyReportData();
        return $this->render('Report/index.html.twig', array(
            'data' => $data
        ));
    }
}
