<?php

namespace AppBundle\Controller;

use AppBundle\Event\EntityEvent;
use AppBundle\Event\FormValidationFailedEvent;
use AppBundle\Event\PageViewedEvent;
use AppBundle\Form\Type\UserType;
use Emicro\UserBundle\Entity\User;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use JMS\SecurityExtraBundle\Annotation as JMS;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends BaseController
{
    /**
     * @Route("/user", name="app_user_list")
     * @JMS\Secure(roles="ROLE_USER_LIST")
     */
    public function indexAction()
    {
        $this->dispatch('page_viewed', new PageViewedEvent("User list"));

        $results = $this->getRepository('EmicroUserBundle:User')->findAll();

        return $this->render('User/index.html.twig', array(
            'results' => $results
        ));
    }

    /**
     * @Route("/user/new", name="app_user_new")
     * @JMS\Secure(roles="ROLE_USER_ADD"))
     */
    public function newAction(Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');

        $permissionManager = $this->get('emicro.user.permission_builder');
        $settingRepo = $this->get('emicro.setting_repo');
        $user = $userManager->createUser();
        $settings = $settingRepo->getSettings();
        $userGroups = $this->getDoctrine()->getRepository('AppBundle:UserGroup')->choiceList('getId', 'getGroupName');

        $form = $this->createForm(new UserType(true, $permissionManager, $userGroups, $request, $settings), $user);
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $user->setForcePasswordChange(true);
                $user->setEnabled(true);
                $userManager->updateUser($user);

                $values = $this->getDoctrine()->getRepository('EmicroUserBundle:User')->getEntityValues($user);
                $this->dispatch('entity.created', new EntityEvent($user, $values));

                $this->get('session')->getFlashBag()
                    ->add('success', 'User Added Successfully');

                return $this->redirect($this->generateUrl('app_user_list'));
            } else {
                $this->dispatch('create.validation.failed', new FormValidationFailedEvent($form));
            }
        } else {
            $this->dispatch('page_viewed', new PageViewedEvent("User New"));
        }

        return $this->render('User/add-edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/user/edit/{id}", name="app_user_edit")
     * @JMS\Secure(roles="ROLE_USER_EDIT"))
     */
    public function editAction(Request $request, User $user)
    {
        $userManager = $this->get('fos_user.user_manager');

        $settingRepo = $this->get('emicro.setting_repo');
        $settings = $settingRepo->getSettings();
        $userGroups = $this->getDoctrine()->getRepository('AppBundle:UserGroup')->choiceList('getId', 'getGroupName');

        $form = $this->createForm(new UserType(false, $this->get('emicro.user.permission_builder'), $userGroups, $request, $settings), clone $user);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $delta = $userManager->getDelta($user, $form->getData());
                $plainPassWord = $form->getData()->getPlainPassWord();


                if (!empty($plainPassWord) || !empty($delta)) {
                    $user = $userManager->updateUser($form->getData());
                    $this->dispatch('entity.updated', new EntityEvent($user, $delta));
                }

                $this->get('session')->getFlashBag()
                    ->add('success', 'User Updated Successfully');

                $response = $this->redirect($this->generateUrl('app_user_list'));

                if (!empty($plainPassWord)) {
                    $this->dispatch(FOSUserEvents::CHANGE_PASSWORD_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
                }

                return $response;

            } else {
                $this->dispatch('edit.validation.failed', new FormValidationFailedEvent($form));
            }
        } else {
            $this->dispatch('page_viewed', new PageViewedEvent("User Edit"));
        }

        return $this->render('User/add-edit.html.twig', array(
            'form' => $form->createView(),
            'user' => $user,
        ));
    }

    /**
     * @Route("/user/delete/{id}", name="app_user_delete")
     * @JMS\Secure(roles="ROLE_USER_DELETE")
     */
    public function deleteAction($id)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $id));

        if (!$user) {
            throw new NotFoundHttpException('Page Not Found');
        }

        $values = $this->getDoctrine()->getRepository('EmicroUserBundle:User')->getEntityValues($user);
        $this->dispatch('entity.deleted', new EntityEvent($user, $values));

        $userManager->deleteUser($user);
        $this->get('session')->getFlashBag()
            ->add('success', 'User Deleted Successfully');

        return $this->redirect($this->generateUrl('app_user_list'));
    }

    /**
     * @Route("/user/lock/{id}", name="app_user_lock")
     * @JMS\Secure(roles="ROLE_USER_LOCK")
     */
    public function lockAction($id)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $id));

        if (!$user) {
            throw new NotFoundHttpException('Page Not Found');
        }

        $this->dispatch('page_viewed', new PageViewedEvent("User Locked: " . $user->getName()));

        $user->setLocked(true);
        $userManager->updateUser($user);
        $this->get('session')->getFlashBag()
            ->add('success', 'User Locked Successfully');

        return $this->redirect($this->generateUrl('app_user_list'));
    }

    /**
     * @Route("/user/unlock/{id}", name="app_user_unlock")
     * @JMS\Secure(roles="ROLE_USER_UNLOCK")
     */
    public function unlockAction($id)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $id));

        if (!$user) {
            throw new NotFoundHttpException('Page Not Found');
        }

        $this->dispatch('page_viewed', new PageViewedEvent("User Unlocked: " . $user->getName()));

        $user->setLocked(false);
        $user->setLockReleaseAt(new \DateTime());
        $userManager->updateUser($user);
        $this->get('session')->getFlashBag()
            ->add('success', 'User Unlock Successfully');

        return $this->redirect($this->generateUrl('app_user_list'));
    }
}
