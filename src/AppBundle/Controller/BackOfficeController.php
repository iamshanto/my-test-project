<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BackOffice;
use AppBundle\Event\EntityEvent;
use AppBundle\Event\FormValidationFailedEvent;
use AppBundle\Event\PageViewedEvent;
use AppBundle\Form\Type\BackOfficeType;
use JMS\SecurityExtraBundle\Annotation as JMS;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BackOfficeController extends BaseController
{
    /**
     * @Route("/back-office", name="app_back_office_list")
     * @JMS\Secure(roles="ROLE_BACK_OFFICE_LIST")
     */
    public function indexAction()
    {
        $this->dispatch('page_viewed', new PageViewedEvent("Back Office List"));

        $results = $this->getDoctrine()->getRepository('AppBundle:BackOffice')->findAll();

        return $this->render('BackOffice/index.html.twig', array(
            'results' => $results
        ));
    }

    /**
     * @Route("/back-office/new", name="app_back_office_new")
     * @JMS\Secure(roles="ROLE_BACK_OFFICE_ADD")
     */
    public function newAction(Request $request)
    {
        $backOffice = new BackOffice();
        $form = $this->createForm(new BackOfficeType(), $backOffice);
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $repo = $this->getDoctrine()->getRepository('AppBundle:BackOffice');
                $repo->save($backOffice);

                $values = $repo->getEntityValues($backOffice);
                $this->dispatch('entity.created', new EntityEvent($backOffice, $values));

                $this->get('session')->getFlashBag()
                    ->add('success', 'Back Office Added Successfully');

                return $this->redirect($this->generateUrl('app_back_office_list'));
            } else {
                $this->dispatch('create.validation.failed', new FormValidationFailedEvent($form));
            }
        } else {
            $this->dispatch('page_viewed', new PageViewedEvent("Back Office Add"));
        }

        return $this->render('BackOffice/add-edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/back-office/edit/{id}", name="app_back_office_edit")
     * @JMS\Secure(roles="ROLE_BACK_OFFICE_EDIT")
     */
    public function editAction(Request $request, BackOffice $backOffice)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:BackOffice');

        $form = $this->createForm(new BackOfficeType(), clone $backOffice);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $delta = $repo->getDelta($backOffice, $form->getData());

                if (null !== $delta) {
                    $repo->save($form->getData());
                    $this->dispatch('entity.updated', new EntityEvent($backOffice, $delta));
                }

                $this->get('session')->getFlashBag()
                    ->add('success', 'Back Office Updated Successfully');

                return $this->redirect($this->generateUrl('app_back_office_list'));
            } else {
                $this->dispatch('edit.validation.failed', new FormValidationFailedEvent($form));
            }
        } else {
            $this->dispatch('page_viewed', new PageViewedEvent("Back Office Edit"));
        }

        return $this->render('BackOffice/add-edit.html.twig', array(
            'form' => $form->createView(),
            'backOffice' => $backOffice,
        ));
    }

    /**
     * @Route("/back-office/delete/{id}", name="app_back_office_delete")
     * @JMS\Secure(roles="ROLE_BACK_OFFICE_DELETE")
     */
    public function deleteAction($id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:BackOffice');
        $backOffice = $repo->find($id);

        if (!$backOffice) {
            throw new NotFoundHttpException('Page Not Found');
        }

        $values = $repo->getEntityValues($backOffice);
        $this->dispatch('entity.deleted', new EntityEvent($backOffice, $values));

        $repo->delete($backOffice);
        $this->get('session')->getFlashBag()
            ->add('success', 'Back Office Deleted Successfully');

        return $this->redirect($this->generateUrl('app_back_office_list'));
    }
}
