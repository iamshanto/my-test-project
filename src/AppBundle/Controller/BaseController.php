<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\Event;

abstract class BaseController extends Controller
{
    protected function dispatch($eventName, Event $event)
    {
        $this->get('event_dispatcher')->dispatch($eventName, $event);
    }

    protected function getRepository($entity)
    {
        return $this->getDoctrine()->getRepository($entity);
    }
}