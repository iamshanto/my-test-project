<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BinInfo;
use AppBundle\Event\EntityEvent;
use AppBundle\Event\FormValidationFailedEvent;
use AppBundle\Event\PageViewedEvent;
use AppBundle\Form\Type\BinInfoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\SecurityExtraBundle\Annotation as JMS;

class BinInfoController extends BaseController
{
    /**
     * @Route("/bin-info", name="app_bin_code_list")
     * @JMS\Secure(roles="ROLE_BININFO_LIST")
     */
    public function indexAction()
    {
        $this->dispatch('page_viewed', new PageViewedEvent("BinInfo list"));

        $results = $this->getRepository('AppBundle:BinInfo')->findAll();

        return $this->render('BinCode/index.html.twig', array(
            'results' => $results
        ));
    }

    /**
     * @Route("/bin-info/new", name="app_bin_code_new")
     * @JMS\Secure(roles="ROLE_BININFO_ADD")
     */
    public function newAction(Request $request)
    {
        $binInfo = new BinInfo();
        $form = $this->createForm(new BinInfoType(), $binInfo);
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $repo = $this->getDoctrine()->getRepository('AppBundle:BinInfo');
                $repo->save($binInfo);

                $this->get('session')->getFlashBag()
                    ->add('success', 'Bin Code Added Successfully');

                $values = $repo->getEntityValues($binInfo);
                $this->dispatch('entity.created', new EntityEvent($binInfo, $values));

                return $this->redirect($this->generateUrl('app_bin_code_list'));
            } else {
                $this->dispatch('create.validation.failed', new FormValidationFailedEvent($form));
            }
        } else {
            $this->dispatch('page_viewed', new PageViewedEvent("bin info add"));
        }

        return $this->render('BinCode/add-edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/bin-info/edit/{id}", name="app_bin_code_edit")
     * @JMS\Secure(roles="ROLE_BININFO_EDIT")
     */
    public function editAction(Request $request, BinInfo $binInfo)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:BinInfo');

        $form = $this->createForm(new BinInfoType(), clone $binInfo);
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $delta = $repo->getDelta($binInfo, $form->getData());

                if (null !== $delta) {
                    $repo->save($form->getData());
                    $this->dispatch('entity.updated', new EntityEvent($binInfo, $delta));
                }

                $this->get('session')->getFlashBag()
                    ->add('success', 'Bin Code Updated Successfully');

                return $this->redirect($this->generateUrl('app_bin_code_list'));
            }else{
                $this->dispatch('edit.validation.failed', new FormValidationFailedEvent($form));
            }
        }else{
            $this->dispatch('page_viewed', new PageViewedEvent("BinInfo edit"));
        }

        return $this->render('BinCode/add-edit.html.twig', array(
            'form' => $form->createView(),
            'binCode' => $binInfo,
        ));
    }

    /**
     * @Route("/bin-info/delete/{id}", name="app_bin_code_delete")
     * @JMS\Secure(roles="ROLE_BININFO_DELETE")
     */
    public function deleteAction($id)
    {

        $repo = $this->getDoctrine()->getRepository('AppBundle:BinInfo');
        $binInfo = $repo->find($id);

        if (!$binInfo) {
            throw new NotFoundHttpException('Page Not Found');
        }

        $values = $repo->getEntityValues($binInfo);
        $this->dispatch('entity.deleted', new EntityEvent($binInfo, $values));

        $repo = $this->getDoctrine()->getRepository('AppBundle:BinInfo');
        $repo->delete($binInfo);

        $this->get('session')->getFlashBag()
            ->add('success', 'Bin Code Deleted Successfully');

        return $this->redirect($this->generateUrl('app_bin_code_list'));
    }
}
