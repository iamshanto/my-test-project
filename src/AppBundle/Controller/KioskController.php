<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Kiosk;
use AppBundle\Event\EntityEvent;
use AppBundle\Event\FormValidationFailedEvent;
use AppBundle\Event\PageViewedEvent;
use AppBundle\Form\Type\KioskType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\SecurityExtraBundle\Annotation as JMS;

class KioskController extends BaseController
{
    /**
     * @Route("/kiosk", name="app_kiosk_list")
     * @JMS\Secure(roles="ROLE_KIOSK_LIST")
     */
    public function indexAction()
    {
        $this->dispatch('page_viewed', new PageViewedEvent("BinInfo list"));

        $results = $this->getDoctrine()->getRepository('AppBundle:Kiosk')->findAll();
        $backOffices = $this->getDoctrine()->getRepository('AppBundle:BackOffice')->choiceList('getId', 'getName');
        $users = $this->getDoctrine()->getRepository('EmicroUserBundle:User')->choiceList('getId', 'getName');

        return $this->render('Kiosk/index.html.twig', array(
            'results' => $results,
            'backOffices' => $backOffices,
            'users' => $users,
        ));
    }

    /**
     * @Route("/kiosk/new", name="app_kiosk_new")
     * @JMS\Secure(roles="ROLE_KIOSK_ADD")
     */
    public function newAction(Request $request)
    {
        $backOffices = $this->getDoctrine()->getRepository('AppBundle:BackOffice')->choiceList('getId', 'getName');
        $users = $this->getDoctrine()->getRepository('EmicroUserBundle:User')->choiceList('getId', 'getName');
        $kiosk = new Kiosk();
        $form = $this->createForm(new KioskType($backOffices, $users), $kiosk);
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $repo = $this->getDoctrine()->getRepository('AppBundle:Kiosk');
                $repo->save($kiosk);

                $values = $repo->getEntityValues($kiosk);
                $this->dispatch('entity.created', new EntityEvent($kiosk, $values));

                $this->get('session')->getFlashBag()
                    ->add('success', 'Kiosk Added Successfully');

                return $this->redirect($this->generateUrl('app_kiosk_list'));
            } else {
                $this->dispatch('create.validation.failed', new FormValidationFailedEvent($form));
            }
        }

        $this->dispatch('page_viewed', new PageViewedEvent("Kiosk add"));

        return $this->render('Kiosk/add-edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/kiosk/edit/{id}", name="app_kiosk_edit")
     * @JMS\Secure(roles="ROLE_KIOSK_EDIT")
     */
    public function editAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Kiosk');
        $kiosk = $repo->find($id);

        if (!$kiosk) {
            throw new NotFoundHttpException('Page Not Found');
        }

        $backOffices = $this->getDoctrine()->getRepository('AppBundle:BackOffice')->choiceList('getId', 'getName');
        $users = $this->getDoctrine()->getRepository('EmicroUserBundle:User')->choiceList('getId', 'getName');
        $form = $this->createForm(new KioskType($backOffices, $users), clone $kiosk);
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $delta = $repo->getDelta($kiosk, $form->getData());

                if (null !== $delta) {
                    $repo->save($form->getData());
                    $this->dispatch('entity.updated', new EntityEvent($kiosk, $delta));
                }

                $this->get('session')->getFlashBag()
                     ->add('success', 'Kiosk Updated Successfully');

                return $this->redirect($this->generateUrl('app_kiosk_list'));
            } else {
                $this->dispatch('edit.validation.failed', new FormValidationFailedEvent($form));
            }
        }

        $this->dispatch('page_viewed', new PageViewedEvent("Kiosk edit"));

        return $this->render('Kiosk/add-edit.html.twig', array(
            'form' => $form->createView(),
            'kiosk' => $kiosk,
        ));
    }

    /**
     * @Route("/kiosk/delete/{id}", name="app_kiosk_delete")
     * @JMS\Secure(roles="ROLE_KIOSK_DELETE")
     */
    public function deleteAction($id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Kiosk');
        $kiosk = $repo->find($id);

        if (!$kiosk) {
            throw new NotFoundHttpException('Page Not Found');
        }

        $repo->delete($kiosk);

        $values = $repo->getEntityValues($kiosk);
        $this->dispatch('entity.deleted', new EntityEvent($kiosk, $values));

        $this->get('session')->getFlashBag()
             ->add('success', 'Kiosk Deleted Successfully');

        return $this->redirect($this->generateUrl('app_kiosk_list'));
    }
}
