<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('Default/index.html.twig', $this->dashboardData());
    }

    private function dashboardData()
    {
        $em = $this->getDoctrine();
        $binInfo = $em->getRepository('AppBundle:BinInfo')->findAll();
        $backOffices = $em->getRepository('AppBundle:BackOffice')->findAll();
        $kiosks = $em->getRepository('AppBundle:Kiosk')->findAll();

        // Live Transaction Data
        $reportRepo = $this->get('appbundle.model.transaction');
        $data = $reportRepo->getLiveTransactionReport();
        $liveTransactions = $this->renderView('Report/Transaction/_live-table.html.twig', array(
            'reportData' => $data['reportData'],
            'reportGrandTotal' => $data['reportGrandTotal'],
            'kiosks' => $em->getRepository('AppBundle:Kiosk')->choiceList('getKioskCode', 'getKioskName')
        ));

        // Kiosk Status Data
        $kiosksTable = $this->renderView(':Report/Kiosk:_index-kiosks-list.html.twig', array(
            'results' => $kiosks
        ));

        return array(
            'totalBackOffices' => count($backOffices),
            'totalBinInfo' => count($binInfo),
            'TotalKiosks' => count($kiosks),
            'liveTransactions' => $liveTransactions,
            'kiosksTable' => $kiosksTable,
        );
    }
}
