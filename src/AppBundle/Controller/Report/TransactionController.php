<?php

namespace AppBundle\Controller\Report;

use AppBundle\Event\PageViewedEvent;
use JMS\SecurityExtraBundle\Annotation as JMS;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/report/transaction")
 */
class TransactionController extends BaseController
{
    /**
     * @Route("/session", name="app_transaction_report_session")
     * @JMS\Secure(roles="ROLE_REPORT_SESSION")
     */
    public function sessionReportAction()
    {
        $this->dispatch('page_viewed', new PageViewedEvent("Session Report"));

        $kiosks = $this->getRepository('AppBundle:Kiosk')->getAllKioskChoiceList();

        return $this->render('Report/Transaction/session.html.twig', array(
            'kiosks' => $kiosks,
        ));
    }

    /**
     * @Route("/session/sessions-by-kiosk", name="app_transaction_report_session_of_kiosk")
     * @JMS\Secure(roles="ROLE_REPORT_SESSION")
     */
    public function sessionByKioskCodeAction(Request $request)
    {
        $date = $request->get('date');
        $kioskCode = $request->get('kioskCode');
        $data = $this->get('appbundle.model.report_filter')->getSessionsByKioskAndDate($kioskCode, $date);

        return new JsonResponse($data);
    }

    /**
     * @Route("/session/report-data", name="app_transaction_report_session_report_data")
     * @JMS\Secure(roles="ROLE_REPORT_SESSION")
     */
    public function sessionReportDataAction(Request $request)
    {
        $this->dispatchReportEvent($request, 'Session Report');

        $reportRepo = $this->get('appbundle.model.transaction');
        $sessionId = $request->get('sessionId');
        $kioskCode = $request->get('kioskCode');
        $kiosk = $this->getRepository('AppBundle:Kiosk')->findOneByKioskCode($kioskCode);
        $data = $reportRepo->getSessionWiseReport($sessionId);

        return $this->render('Report/Transaction/_session-table.html.twig', array(
            'reportData' => $data['reportData'],
            'reportGrandTotal' => $data['reportGrandTotal'],
            'sessionStartEnd' => $data['sessionStartEnd'],
            'kiosk' => $kiosk,
            'sessionId' => $sessionId,
        ));
    }

    /**
     * @Route("/daily", name="app_transaction_report_daily")
     * @JMS\Secure(roles="ROLE_REPORT_DAILY")
     */
    public function dailyReportAction()
    {
        $this->dispatch('page_viewed', new PageViewedEvent("Daily Report"));

        $kiosks = $this->getRepository('AppBundle:Kiosk')->getAllKioskChoiceList();

        return $this->render('Report/Transaction/daily.html.twig', array(
            'kiosks' => $kiosks,
        ));
    }

    /**
     * @Route("/daily/report-data", name="app_transaction_report_daily_report_data")
     * @JMS\Secure(roles="ROLE_REPORT_DAILY")
     */
    public function dailyReportDataAction(Request $request)
    {
        $this->dispatchReportEvent($request, 'Daily Report');

        $reportRepo = $this->get('appbundle.model.transaction');
        $kioskCode = $request->get('kioskCode');
        $kiosk = $this->getRepository('AppBundle:Kiosk')->findOneByKioskCode($kioskCode);
        list($startDate, $endDate) = array_filter(explode(' to ', $request->get('date')));

        $data = $reportRepo->getDailyTransactions($kioskCode, $startDate, $endDate);

        return new JsonResponse(array(
            'html' => $this->renderView('Report/Transaction/_daily-table.html.twig', array(
                'reportData' => $data['reportData'],
                'reportGrandTotal' => $data['reportGrandTotal'],
                'kiosk' => $kiosk
            )),
            'chartData' => $data['chartData'],
        ));
    }

    /**
     * @Route("/monthly", name="app_transaction_report_monthly")
     * @JMS\Secure(roles="ROLE_REPORT_MONTHLY")
     */
    public function monthlyReportAction()
    {
        $this->dispatch('page_viewed', new PageViewedEvent("Monthly Report"));
        $kiosks = $this->getRepository('AppBundle:Kiosk')->getAllKioskChoiceList();

        return $this->render('Report/Transaction/monthly.html.twig', array(
            'kiosks' => $kiosks,
        ));
    }

    /**
     * @Route("/monthly/report-data", name="app_transaction_report_month_report_data")
     * @JMS\Secure(roles="ROLE_REPORT_MONTHLY")
     */
    public function monthlyReportDataAction(Request $request)
    {
        $this->dispatchReportEvent($request, 'Monthly Report');

        $reportRepo = $this->get('appbundle.model.transaction');
        $kioskCode = $request->get('kioskCode');
        $kiosk = $this->getRepository('AppBundle:Kiosk')->findOneByKioskCode($kioskCode);
        $startDate = (new \DateTime($request->query->get('start-month').' '.$request->query->get('start-year')))->format("Y-m-d H:i:s");
        $endDate = (new \DateTime($request->query->get('end-month').' '.$request->query->get('end-year')))->format("Y-m-d H:i:s");

        $data = $reportRepo->getMonthlyTransactions($kioskCode, $startDate, $endDate);

        return new JsonResponse(array(
            'html' => $this->renderView('Report/Transaction/_monthly-table.html.twig', array(
                'reportData' => $data['reportData'],
                'reportGrandTotal' => $data['reportGrandTotal'],
                'kiosk' => $kiosk
            )),
            'chartData' => $data['chartData']
        ));
    }

    /**
     * @Route("/yearly", name="app_transaction_report_yearly")
     * @JMS\Secure(roles="ROLE_REPORT_YEARLY")
     */
    public function yearlyReportAction()
    {
        $this->dispatch('page_viewed', new PageViewedEvent("Yearly Report"));
        $kiosks = $this->getRepository('AppBundle:Kiosk')->getAllKioskChoiceList();

        return $this->render('Report/Transaction/yearly.html.twig', array(
            'kiosks' => $kiosks,
        ));
    }

    /**
     * @Route("/yearly/report-data", name="app_transaction_report_yearly_report_data")
     * @JMS\Secure(roles="ROLE_REPORT_YEARLY")
     */
    public function yearlyReportDataAction(Request $request)
    {
        $this->dispatchReportEvent($request, 'Yearly Report');

        $reportRepo = $this->get('appbundle.model.transaction');
        $kioskCode = $request->get('kioskCode');
        $kiosk = $this->getRepository('AppBundle:Kiosk')->findOneByKioskCode($kioskCode);
        $startDate = $request->query->get('start-year');
        $endDate = $request->query->get('end-year');

        $data = $reportRepo->getYearlyTransactions($kioskCode, $startDate, $endDate);

        return new JsonResponse(array(
            'html' => $this->renderView('Report/Transaction/_yearly-table.html.twig', array(
                'reportData' => $data['reportData'],
                'reportGrandTotal' => $data['reportGrandTotal'],
                'kiosk' => $kiosk
            )),
            'chartData' => $data['chartData']
        ));
    }

    /**
     * @Route("/live", name="app_transaction_report_live")
     * @JMS\Secure(roles="ROLE_REPORT_LIVE")
     */
    public function liveReportAction()
    {
        $this->dispatch('page_viewed', new PageViewedEvent("Live Transactions"));

        return $this->render('Report/Transaction/live.html.twig');
    }

    /**
     * @Route("/live/report-data", name="app_transaction_report_live_report_date")
     * @JMS\Secure(roles="ROLE_REPORT_LIVE")
     */
    public function liveReportDataAction(Request $request)
    {
        $this->dispatchReportEvent($request, 'Live Report');

        $kiosks = $this->getRepository('AppBundle:Kiosk')->choiceList('getKioskCode', 'getKioskName');
        $reportRepo = $this->get('appbundle.model.transaction');
        $data = $reportRepo->getLiveTransactionReport();

        return $this->render('Report/Transaction/_live-table.html.twig', array(
            'reportData' => $data['reportData'],
            'reportGrandTotal' => $data['reportGrandTotal'],
            'kiosks' => $kiosks
        ));
    }
}