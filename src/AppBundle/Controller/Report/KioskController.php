<?php

namespace AppBundle\Controller\Report;

use AppBundle\Event\PageViewedEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\SecurityExtraBundle\Annotation as JMS;

/**
 * @Route("/report/kiosk-status")
 */
class KioskController extends BaseController
{
    /**
     * @Route("", name="app_kiosk_status_list")
     * @JMS\Secure(roles="ROLE_KIOSK_STATUS")
     */
    public function indexAction()
    {
        $this->dispatch('page_viewed', new PageViewedEvent("Kiosk Status Report"));

        return $this->render('Report/Kiosk/index.html.twig', array(
            'results' => $this->getDoctrine()->getRepository('AppBundle:Kiosk')->findAll()
        ));
    }

    /**
     * @Route("/refresh", name="app_kiosk_status")
     * @JMS\Secure(roles="ROLE_KIOSK_STATUS")
     */
    public function statusAction()
    {
        $this->dispatch('page_viewed', new PageViewedEvent("Kiosk Status Check"));

        $settings = $this->getRepository('AppBundle:Setting')->getSettings();

        $results = $this->getDoctrine()->getRepository('AppBundle:Kiosk')->getHeartBitElapsedTime($settings['heart_beat_tolerance']);

        return new JsonResponse($results);
    }
}