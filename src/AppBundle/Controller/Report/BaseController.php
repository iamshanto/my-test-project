<?php

namespace AppBundle\Controller\Report;

use AppBundle\Controller\BaseController as ReportBaseController;
use AppBundle\Event\ReportViewEvent;
use Symfony\Component\HttpFoundation\Request;

class BaseController extends ReportBaseController
{
    protected function dispatchReportEvent(Request $request, $type)
    {
        $this->dispatch('report_viewed', new ReportViewEvent($request->query->all(), $type));
    }
}