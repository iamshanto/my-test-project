<?php

namespace AppBundle\Controller\Report;

use AppBundle\Event\PageViewedEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\SecurityExtraBundle\Annotation as JMS;

/**
 * @Route("/report/audit-log")
 */
class AuditLogController extends BaseController
{
    /**
     * @Route("", name="app_audit_log")
     * @JMS\Secure(roles="ROLE_REPORT_AUDIT")
     */
    public function indexAction()
    {
        $this->dispatch('page_viewed', new PageViewedEvent('Audit Log Report'));

        $users = $this->get('appbundle.model.report_filter')->getAuditLogUsers();

        return $this->render('Report/AuditLog/index.html.twig', array(
            'users' => $users
        ));
    }

    /**
     * @Route("/report-data", name="app_audit_log_data")
     * @JMS\Secure(roles="ROLE_REPORT_AUDIT")
     */
    public function reportDataAction(Request $request)
    {
        $this->dispatchReportEvent($request, 'Audit Log Report');

        $reportRepo = $this->get('appbundle.model.audit_log');
        list($startDate, $endDate) = array_filter(explode(' to ', $request->get('date')));
        $user = $request->query->get('user', null);
        $data = $reportRepo->getLogByDateAndUser($startDate, $endDate, $user);

        return $this->render('Report/AuditLog/_index-table.html.twig', array(
            'data' => $data
        ));
    }

    /**
     * @Route("/kiosk", name="app_audit_log_kiosk")
     * @JMS\Secure(roles="ROLE_REPORT_KIOSK_AUDIT")
     */
    public function kioskAuditLogAction()
    {
        $this->dispatch('page_viewed', new PageViewedEvent('Audit Log Report'));

        $reportRepo = $this->get('appbundle.model.audit_log');
        $kiosks = $this->getRepository('AppBundle:Kiosk')->choiceList('getKioskCode', 'getKioskName');

        return $this->render('Report/AuditLog/kiosk.html.twig', array(
            'kiosks' => $kiosks,
            'filterOption' => $reportRepo->getKioskFilterOptions()
        ));
    }

    /**
     * @Route("/kiosk/report-data", name="app_audit_log_kiosk_data")
     * @JMS\Secure(roles="ROLE_REPORT_KIOSK_AUDIT")
     */
    public function kioskAuditLogDataAction(Request $request)
    {
        $this->dispatch('page_viewed', new PageViewedEvent('Audit Log Report'));

        $data = $request->query->all();
        $reportRepo = $this->get('appbundle.model.audit_log');
        $kiosks = $this->getRepository('AppBundle:Kiosk')->findAll();
        list($startDate, $endDate) = array_filter(explode(' to ', $data['date']));

        $data = $reportRepo->getKioskAuditLog(
            $startDate, $endDate,
            $request->query->get('kioskCode'),
            $request->query->get('filterBy'),
            $request->query->get('filterValue')
        );

        return $this->render('Report/AuditLog/_kiosk-table.html.twig', array(
            'data' => $data,
            'kiosks' => $kiosks,
            'filterOption' => $reportRepo->getKioskFilterOptions()
        ));
    }
}