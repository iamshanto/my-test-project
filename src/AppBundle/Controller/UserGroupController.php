<?php

namespace AppBundle\Controller;

use AppBundle\Entity\UserGroup;
use AppBundle\Event\EntityEvent;
use AppBundle\Event\FormValidationFailedEvent;
use AppBundle\Event\PageViewedEvent;
use AppBundle\Form\Type\UserGroupType;
use JMS\SecurityExtraBundle\Annotation as JMS;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserGroupController extends BaseController
{
    /**
     * @Route("/user/group", name="app_user_group_list")
     * @JMS\Secure(roles="ROLE_USER_GROUP_LIST")
     */
    public function indexAction()
    {
        $this->dispatch('page_viewed', new PageViewedEvent("User Group list"));

        $results = $this->getDoctrine()->getRepository('AppBundle:UserGroup')->findAll();

        return $this->render('UserGroup/index.html.twig', array(
            'results' => $results
        ));
    }

    /**
     * @Route("/user/group/new", name="app_user_group_new")
     * @JMS\Secure(roles="ROLE_USER_GROUP_ADD")
     */
    public function newAction(Request $request)
    {
        $userGroup = new UserGroup();
        $form = $this->createForm(new UserGroupType(), $userGroup);
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $repo = $this->getDoctrine()->getRepository('AppBundle:UserGroup');
                $repo->save($userGroup);

                $values = $repo->getEntityValues($userGroup);
                $this->dispatch('entity.created', new EntityEvent($userGroup, $values));
                $this->get('session')->getFlashBag()
                    ->add('success', 'User Group Added Successfully');

                return $this->redirect($this->generateUrl('app_user_group_list'));
            } else {
                $this->dispatch('create.validation.failed', new FormValidationFailedEvent($form));
            }
        } else {
            $this->dispatch('page_viewed', new PageViewedEvent("User Group Add"));
        }

        return $this->render('UserGroup/add-edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/user/group/edit/{id}", name="app_user_group_edit")
     * @JMS\Secure(roles="ROLE_USER_GROUP_EDIT")
     */
    public function editAction(Request $request, UserGroup $userGroup)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:UserGroup');

        $form = $this->createForm(new UserGroupType(), clone $userGroup);
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $delta = $repo->getDelta($userGroup, $form->getData());

                if (null !== $delta) {
                    $repo->save($form->getData());
                    $this->dispatch('entity.updated', new EntityEvent($userGroup, $delta));
                }

                $this->get('session')->getFlashBag()
                     ->add('success', 'User Group Updated Successfully');

                return $this->redirect($this->generateUrl('app_user_group_list'));
            } else {
                $this->dispatch('edit.validation.failed', new FormValidationFailedEvent($form));
            }
        } else {
            $this->dispatch('page_viewed', new PageViewedEvent("User Group Edit"));
        }

        return $this->render('UserGroup/add-edit.html.twig', array(
            'form' => $form->createView(),
            'userGroup' => $userGroup,
        ));
    }

    /**
     * @Route("/user/group/delete/{id}", name="app_user_group_delete")
     * @JMS\Secure(roles="ROLE_USER_GROUP_DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getEntityManager();
        $userGroup = $repo->find('AppBundle:UserGroup', $id);

        if (!$userGroup) {
            throw new NotFoundHttpException('Page Not Found');
        }

        $values = $repo->getEntityValues($userGroup);
        $this->dispatch('entity.deleted', new EntityEvent($userGroup, $values));

        $repo->remove($userGroup);
        $repo->flush();
        $this->get('session')->getFlashBag()
             ->add('success', 'Back Office Deleted Successfully');

        return $this->redirect($this->generateUrl('app_user_group_list'));
    }
}
