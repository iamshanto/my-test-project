<?php

namespace AppBundle\Subscriber;

use Xiidea\EasyAuditBundle\Subscriber\EasyAuditEventSubscriberInterface;

class AuditLogEventSubscriber implements EasyAuditEventSubscriberInterface
{
    public function getSubscribedEvents()
    {
        return array(
            "user.event_resolver" => array(
                "security.interactive_login",
                "security.authentication.failure",
                "fos_user.change_password.edit.completed",
                "fos_user.security.implicit_login"
            ),
            "access.denied.event",
            "security.api.authenticated",
            "report_viewed",
            "setting.changed",
            "page_viewed",
            "create.validation.failed",
            "edit.validation.failed",
            "entity.created",
            "entity.updated",
            "entity.deleted",
        );
    }
}