<?php

namespace AppBundle\Listener;

use AppBundle\Event\AccessDeniedEvent;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcher;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AccessDeniedExceptionListener
{
    /** @var  TraceableEventDispatcher */
    private $dispatcher;

    public function onAccessDeniedException(GetResponseForExceptionEvent $event)
    {
        switch ($event->getException()->getMessage()) {
            case 'Access Denied':
            case 'Token does not have the required roles.':
                $this->dispatcher->dispatch('access.denied.event', new AccessDeniedEvent($event->getRequest()));
                $event->setException(new HttpException(403, 'User does not have sufficient privileges!'));
                break;
        }
    }

    /**
     * @param TraceableEventDispatcher $dispatcher
     */
    public function setDispatcher($dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }
}