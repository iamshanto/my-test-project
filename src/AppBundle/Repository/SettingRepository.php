<?php

namespace AppBundle\Repository;


class SettingRepository extends EntityRepository
{
    const SETTING_SAVE_SP_QUERY = 'SETTINGS_SAVE(:sp_out_status, :sp_out_message, :name, :value)';
    const SETTING_FIND_ALL_SP_QUERY = 'SETTINGS_FIND_ALL(:results)';

    private $_settingsCache = array();

    public function saveSettingByName($name, $value)
    {
        $sql = self::SETTING_SAVE_SP_QUERY;

        $stmt = $this->prepareStoredProcedureStatement($sql);

        oci_bind_by_name($stmt, ":sp_out_status", $statusOut, 32);
        oci_bind_by_name($stmt, ":sp_out_message", $statusMsgOut, 500);
        oci_bind_by_name($stmt, ":name", $name, 500);
        oci_bind_by_name($stmt, ":value", $value, 500);
        oci_execute($stmt);

        $this->_settingsCache[$name] = $value;
    }

    public function saveAll($data)
    {
        foreach ($data as $field => $value) {
            $this->saveSettingByName($field, $value);
        }
    }

    public function getSettingByName($name, $default = null)
    {
        if (empty($this->_settingsCache)) {
            $settings = $this->getSettings();
        }

        return isset($settings[$name]) ? $settings[$name] : $default;
    }

    public function getSettings($cached = true)
    {
        if ($cached && !empty($this->_settingsCache)) {
            return $this->_settingsCache;
        }

        try {
            $connection = $this->getConnectionResource();

            $sql = self::SETTING_FIND_ALL_SP_QUERY;

            $stmt = $this->prepareStoredProcedureStatement($sql);

            $results = oci_new_cursor($connection);
            oci_bind_by_name($stmt, ":results", $results, -1, OCI_B_CURSOR);
            oci_execute($stmt);
            oci_execute($results);

            while (($row = oci_fetch_array($results, OCI_ASSOC + OCI_RETURN_NULLS)) != false) {
                $this->_settingsCache[$row['NAME']] = $row['VALUE'];
            }

        } catch (\Exception $e) {
            var_dump($e->getMessage());
            exit;
        }

        return $this->_settingsCache;
    }
}
