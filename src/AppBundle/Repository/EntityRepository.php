<?php

namespace AppBundle\Repository;

use AppBundle\Traits\OracleConnection;
use Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Symfony\Component\PropertyAccess\PropertyAccess;

class EntityRepository extends BaseEntityRepository
{
    use OracleConnection;

    protected $_entityClassMeta;

    const FIND_BY_ID_SP_QUERY = "%s_FIND(:id, :results)";
    const FIND_ALL_SP_QUERY = "%s_FIND_ALL(:results)";
    const SAVE_SP_QUERY = "%s_SAVE(:sp_out_status, :sp_out_message, :%s)";
    const DELETE_SP_QUERY = "%s_DELETE(:id)";

    public function save($entity)
    {
        $fields = $this->getClassMetadata()->getFieldNames();

        $sql = sprintf(self::SAVE_SP_QUERY, $this->getTableName(), implode(", :", $fields));

        $stmt = $this->prepareStoredProcedureStatement($sql);

        oci_bind_by_name($stmt, ":sp_out_status", $statusOut, 32);
        oci_bind_by_name($stmt, ":sp_out_message", $statusMsgOut, 500);

        $accessor = PropertyAccess::createPropertyAccessor();

        $idField = null;
        $dataArray = array();
        foreach ($fields as $field) {

            $dataArray[$field] = $accessor->getValue($entity, $field);

            if ($this->getClassMetadata()->isIdentifier($field)) {
                $idField = $field;
            }

            $this->bindFieldValue($stmt, $field, $dataArray, $idField);
        }

        oci_execute($stmt);

        if ($statusOut === '1') {
            $accessor->setValue($entity, $idField, $dataArray[$idField]);
            return $entity;
        }

        return null;
    }

    public function getDelta($old, $new)
    {
        $fields = $this->getClassMetadata()->getFieldNames();
        $accessor = PropertyAccess::createPropertyAccessor();
        $changedKey = array();

        foreach ($fields as $field) {
            $oldValue = $accessor->getValue($old, $field);
            if ($oldValue != $accessor->getValue($new, $field)) {
                $changedKey[$field] = $oldValue;
            }
        }

        if(empty($changedKey)){
            $changedKey = null;
        }

        return $changedKey;
    }

    public function find($id)
    {
        return $this->hydrateToObject($this->getEntityAsArray($id));
    }

    public function findAll()
    {

        $entityCollection = array();

        try {
            $connection = $this->getConnectionResource();

            $sql = sprintf(self::FIND_ALL_SP_QUERY, $this->getTableName());

            $stmt = $this->prepareStoredProcedureStatement($sql);

            $results = oci_new_cursor($connection);
            oci_bind_by_name($stmt, ":results", $results, -1, OCI_B_CURSOR);
            oci_execute($stmt);
            oci_execute($results);

            while (($row = oci_fetch_array($results, OCI_ASSOC + OCI_RETURN_NULLS)) != false) {
                $entityCollection[] = $this->hydrateToObject($row);
            }

        } catch (\Exception $e) {
            $entityCollection = parent::findAll();
        }

        return $entityCollection;

    }

    public function delete($entity)
    {
        $id = is_object($entity) ? $entity->getId() : $entity;

        $stmt = $this->prepareStoredProcedureStatement(
            sprintf(self::DELETE_SP_QUERY, $this->getTableName())
        );

        oci_bind_by_name($stmt, ":id", $id);
        oci_execute($stmt);
    }

    /**
     * @param $id
     * @return array|null
     */
    protected function getEntityAsArray($id)
    {
        $connection = $this->getConnectionResource();

        $sql = sprintf(self::FIND_BY_ID_SP_QUERY, $this->getTableName());

        $stmt = $this->prepareStoredProcedureStatement($sql);

        $results = oci_new_cursor($connection);
        oci_bind_by_name($stmt, ":id", $id, 32);
        oci_bind_by_name($stmt, ":results", $results, -1, OCI_B_CURSOR);
        oci_execute($stmt);
        oci_execute($results);

        while (($row = oci_fetch_array($results, OCI_ASSOC + OCI_RETURN_NULLS)) != false) {
            return $row;
        }

        return null;
    }

    /**
     * @param array $entityArray
     * @return null
     */
    protected function hydrateToObject(array $entityArray = array())
    {
        if (empty($entityArray)) {
            return null;
        }

        $entityObject = new $this->_entityName;

        $accessor = PropertyAccess::createPropertyAccessor();

        foreach ($entityArray as $key => $value) {
            $accessor->setValue($entityObject, $this->getClassMetadata()->getFieldName($key), $value);
        }

        return $entityObject;
    }

    /**
     * @return string
     */
    protected function getTableName()
    {
        return $this->getClassMetadata()->getTableName();
    }

    /**
     * @param $stmt
     * @param $field
     * @param $dataArray
     * @param $idField
     */
    protected function bindFieldValue($stmt, $field, &$dataArray, $idField)
    {
        $maxLength = ($idField == $field && empty($dataArray[$field])) ? 32 : -1;
        $dataArray[$field] = $this->resolveValue($dataArray[$field]);
        oci_bind_by_name($stmt, ":$field", $dataArray[$field], $maxLength);
    }

    public function resolveValue($value)
    {
        if ($value instanceof \DateTime) {
            return $value->format('Y-m-d H:i:s');
        }

        return $value;
    }

    public function choiceList($keyField, $valueField)
    {
        $results = array();
        foreach ($this->findAll() as $entity) {
            $results[$entity->$keyField()] = $entity->$valueField();
        }

        return $results;
    }

    public function getEntityValues($entity)
    {
        $fields = $this->getClassMetadata()->getFieldNames();
        $accessor = PropertyAccess::createPropertyAccessor();
        $values = array();

        // /vendor/symfony/symfony/src/Symfony/Component/Form/FormRenderer.php
        foreach ($fields as $field) {
            $fieldName = ucfirst(trim(strtolower(preg_replace(array('/([A-Z])/', '/[_\s]+/'), array('_$1', ' '), $field))));
            $values[$fieldName] = $accessor->getValue($entity, $field);
        }

        return $values;
    }

}