<?php
namespace AppBundle\Repository;

use Doctrine\DBAL\Connection;

class ReportRepository {

    private $db;
    public function __construct(Connection $connection)
    {
        $this->db = $connection;
    }

    public function reportData($type)
    {
        $data = array();

        for ($i = 0; $i < 10; $i++) {
            $row = array(
                'deposit' => rand(5000, 20000),
                'kiosk' => 'A'.$i,
            );
            switch ($type) {
                case 'yearly':
                    $row['date'] = 2005 + $i;
                    break;
                case 'monthly':
                    $row['date'] = date("Y F", mktime(0,0,0, $i + 1, 1, 2015));
                    break;
                case 'daily':
                    $row['date'] = date("d F Y", mktime(0,0,0, 4, $i + 1, 2015));
                    break;
            }
            $data[] = $row;
        }

        return $data;
    }

    public function yearlyReportData()
    {
        return $this->reportData('yearly');
    }

    public function monthlyReportData()
    {
        return $this->reportData('monthly');
    }

    public function dailyReportData()
    {
        return $this->reportData('daily');
    }

}