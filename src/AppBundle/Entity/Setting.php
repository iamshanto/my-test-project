<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SettingRepository")
 * @ORM\Table(name="Settings")
 */
class Setting
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="IID")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Type Of Event(Internal Type ID)
     *
     * @var string
     * @ORM\Column(name="NAME", type="string", length=200, nullable=false)
     */
    protected $name;

    /**
     * Type Of Event
     *
     * @var string
     * @ORM\Column(name="VALUE", type="string", length=200, nullable=true)
     */
    protected $value;

    /**
     * @var string
     * @ORM\Column(name="OPTIONS", type="string", length=1200, nullable=true)
     */
    protected $options;

    /**
     * @return mixed
     */
    public function getId ()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId ($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName ()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName ($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue ()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue ($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getOptions ()
    {
        return json_decode($this->options, true);
    }

    /**
     * @param string $options
     */
    public function setOptions ($options)
    {
        $this->options = json_encode($options);

        return $this;
    }

}