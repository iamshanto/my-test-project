<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * BackOffice
 *
 * @ORM\Table(name="BACKOFFICEINFO")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BackOfficeRepository")
 */
class BackOffice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="BACKOFFICENAME", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="BACKOFFICEADDRESS", type="text")
     */
    private $address;

    public function __construct() {
        $this->kiosks = new ArrayCollection();
        $this->sessions = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BackOffice
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return BackOffice
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param int $id
     * @return BackOffice
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

}
