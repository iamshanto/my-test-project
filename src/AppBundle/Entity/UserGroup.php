<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BinInfo
 *
 * @ORM\Table(name="USER_GROUP")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserGroupRepository")
 */
class UserGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="GROUPNAME", type="string", length=50)
     */
    private $groupName;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getGroupName ()
    {
        return $this->groupName;
    }

    /**
     * @param string $groupName
     */
    public function setGroupName ($groupName)
    {
        $this->groupName = $groupName;
    }

}