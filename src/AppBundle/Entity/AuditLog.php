<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Xiidea\EasyAuditBundle\Entity\BaseAuditLog;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EntityRepository")
 * @ORM\Table(name="APP_AUDIT_LOG")
 */
class AuditLog extends BaseAuditLog
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", name="IID")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Type Of Event(Internal Type ID)
     *
     * @var string
     * @ORM\Column(name="TYPE_ID", type="string", length=200, nullable=false)
     */
    protected $typeId;

    /**
     * Type Of Event
     *
     * @var string
     * @ORM\Column(name="TYPE_NAME", type="string", length=200, nullable=true)
     */
    protected $type;

    /**
     * @var string
     * @ORM\Column(name="DESCRIPTION", type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * Time Of Event
     * @var \DateTime
     * @ORM\Column(name="EVENT_TIME", type="datetime")
     */
    protected $eventTime;

    /**
     * @var string
     * @ORM\Column(name="USER_NAME", type="string", length=255)
     */
    protected $user;

    /**
     * @var string
     * @ORM\Column(name="IP_ADDRESS", type="string", length=20, nullable=true)
     */
    protected $ip;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}