<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SessionInfo
 *
 * @ORM\Table(name="SESSIONINFO")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SessionInfoRepository")
 */
class SessionInfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="SESSIONIID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\BackOffice", inversedBy="sessions")
     * @ORM\JoinColumn(name="BACKOFFICEID", referencedColumnName="IID")
     **/
    private $backOffice;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Kiosk", inversedBy="sessions")
     * @ORM\JoinColumn(name="KIOSKCODE", referencedColumnName="IID")
     **/
    private $kiosk;


    /**
     * @var string
     *
     * @ORM\Column(name="SESSIONID", type="string", length=255)
     */
    private $sessionId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="SESSIONDATETIME", type="datetime")
     */
    private $sessionDateTime;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set backOffice
     *
     * @param string $backOffice
     * @return SessionInfo
     */
    public function setBackOffice($backOffice)
    {
        $this->backOffice = $backOffice;

        return $this;
    }

    /**
     * Get backOffice
     *
     * @return string 
     */
    public function getBackOffice()
    {
        return $this->backOffice;
    }

    /**
     * Set kiosk
     *
     * @param string $kiosk
     * @return SessionInfo
     */
    public function setKiosk($kiosk)
    {
        $this->kiosk = $kiosk;

        return $this;
    }

    /**
     * Get kiosk
     *
     * @return string 
     */
    public function getKiosk()
    {
        return $this->kiosk;
    }

    /**
     * Set sessionId
     *
     * @param string $sessionId
     * @return SessionInfo
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get sessionId
     *
     * @return string 
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * Set sessionDateTime
     *
     * @param \DateTime $sessionDateTime
     * @return SessionInfo
     */
    public function setSessionDateTime($sessionDateTime)
    {
        $this->sessionDateTime = $sessionDateTime;

        return $this;
    }

    /**
     * Get sessionDateTime
     *
     * @return \DateTime 
     */
    public function getSessionDateTime()
    {
        return $this->sessionDateTime;
    }
}
