<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BinInfo
 *
 * @ORM\Table(name="BININFO")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BinInfoRepository")
 */
class BinInfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="BINCODE", type="string", length=50)
     */
    private $binCode;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPENAME", type="string", length=50)
     */
    private $typeName;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId ($id)
    {
        $this->id = $id;
    }


    /**
     * Set binCode
     *
     * @param string $binCode
     * @return BinInfo
     */
    public function setBinCode($binCode)
    {
        $this->binCode = $binCode;

        return $this;
    }

    /**
     * Get binCode
     *
     * @return string 
     */
    public function getBinCode()
    {
        return $this->binCode;
    }

    /**
     * Set typeName
     *
     * @param string $typeName
     * @return BinInfo
     */
    public function setTypeName($typeName)
    {
        $this->typeName = $typeName;

        return $this;
    }

    /**
     * Get typeName
     *
     * @return string 
     */
    public function getTypeName()
    {
        return $this->typeName;
    }
}
