<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kiosk
 *
 * @ORM\Table(name="KIOSKCONFIGINFO")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\KioskRepository")
 */
class Kiosk
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="KIOSKCODE", type="string", length=50)
     */
    private $kioskCode;

    /**
     * @var string
     *
     * @ORM\Column(name="KIOSKNAME", type="string", length=50)
     */
    private $kioskName;

    /**
     * @var string
     *
     * @ORM\Column(name="INSURANCEAMOUNT", type="decimal")
     */
    private $insuranceAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="BANKNAME", type="string", length=100)
     */
    private $bankName;

    /**
     * @var string
     *
     * @ORM\Column(name="BRANCHNAME", type="string", length=100)
     */
    private $branchName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="HEARTBITDATETIME", type="datetime")
     */
    private $heartBitDateTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="BACKOFFICEID", type="integer")
     */
    private $backOfficeId;

    /**
     * @var string
     *
     * @ORM\Column(name="ADDRESS", type="string", length=500)
     */
    private $address;

    public function __construct()
    {
        if (!$this->getId()) {
            $this->setHeartBitDateTime(new \DateTime());
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId ($id)
    {
        $this->id = $id;
    }

    /**
     * Set kioskCode
     *
     * @param string $kioskCode
     * @return Kiosk
     */
    public function setKioskCode($kioskCode)
    {
        $this->kioskCode = $kioskCode;

        return $this;
    }

    /**
     * Get kioskCode
     *
     * @return string 
     */
    public function getKioskCode()
    {
        return $this->kioskCode;
    }

    /**
     * Set kioskName
     *
     * @param string $kioskName
     * @return Kiosk
     */
    public function setKioskName($kioskName)
    {
        $this->kioskName = $kioskName;

        return $this;
    }

    /**
     * Get kioskName
     *
     * @return string 
     */
    public function getKioskName()
    {
        return $this->kioskName;
    }

    /**
     * Set insuranceAmount
     *
     * @param string $insuranceAmount
     * @return Kiosk
     */
    public function setInsuranceAmount($insuranceAmount)
    {
        $this->insuranceAmount = $insuranceAmount;

        return $this;
    }

    /**
     * Get insuranceAmount
     *
     * @return string 
     */
    public function getInsuranceAmount()
    {
        return $this->insuranceAmount;
    }

    /**
     * Set bankName
     *
     * @param string $bankName
     * @return Kiosk
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;

        return $this;
    }

    /**
     * Get bankName
     *
     * @return string 
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * Set branchName
     *
     * @param string $branchName
     * @return Kiosk
     */
    public function setBranchName($branchName)
    {
        $this->branchName = $branchName;

        return $this;
    }

    /**
     * Get branchName
     *
     * @return string 
     */
    public function getBranchName()
    {
        return $this->branchName;
    }

    /**
     * Set heartBitDateTime
     *
     * @param \DateTime $heartBitDateTime
     * @return Kiosk
     */
    public function setHeartBitDateTime($heartBitDateTime)
    {
        $this->heartBitDateTime = $heartBitDateTime;

        return $this;
    }

    /**
     * Get heartBitDateTime
     *
     * @return \DateTime 
     */
    public function getHeartBitDateTime()
    {
        return $this->heartBitDateTime;
    }

    /**
     * Set backOffice
     *
     * @param string $backOffice
     * @return Kiosk
     */
    public function setBackOffice($backOffice)
    {
        $this->backOffice = $backOffice;

        return $this;
    }

    /**
     * @return int
     */
    public function getBackOfficeId ()
    {
        return $this->backOfficeId;
    }

    /**
     * @param int $backOfficeId
     */
    public function setBackOfficeId ($backOfficeId)
    {
        $this->backOfficeId = $backOfficeId;
    }

    /**
     * @return int
     */
    public function getUserId ()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId ($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getAddress ()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress ($address)
    {
        $this->address = $address;
    }

}