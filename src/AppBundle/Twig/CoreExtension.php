<?php

namespace AppBundle\Twig;


use AppBundle\Repository\SettingRepository;

class CoreExtension extends \Twig_Extension
{
    /**
     * @var SettingRepository
     */
    private $settingRepository;

    public function __construct(SettingRepository $settingRepository){

        $this->settingRepository = $settingRepository;
    }

    /**
     * @return array
     */
    public function getGlobals()
    {
        return array(
            'appSettings' => $this->settingRepository->getSettings(false)
        );
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     *
     */
    public function getName()
    {
        return 'cdm_app_core_extension';
    }
}