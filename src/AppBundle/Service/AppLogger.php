<?php
namespace AppBundle\Service;

use Xiidea\EasyAuditBundle\Entity\BaseAuditLog;
use Xiidea\EasyAuditBundle\Logger\Logger;
use Xiidea\EasyAuditBundle\Logger\LoggerInterface;

class AppLogger extends Logger implements LoggerInterface
{
    public function log(BaseAuditLog $event)
    {
        if(empty($event)) {
            return;
        }

        $this->getRepository()->save($event);
    }

    /**
     * @return \AppBundle\Repository\EntityRepository
     */
    protected function getRepository()
    {
        return $this->getEntityManager()->getRepository("AppBundle:AuditLog");
    }
}