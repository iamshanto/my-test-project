<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Setting;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class SettingFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $defaultConstrains = array(new NotBlank());
        $formOption = array(
            'constraints' => $defaultConstrains
        );

        /** @var Setting $setting */
        foreach ($options['data']['settings'] as $setting) {
            $option = $setting->getOptions();
            $formOption['data'] = $setting->getValue();
            if (!is_array($option)) {
                $option = json_decode($option, true);
            }
            $builder->add($setting->getName(), $option['type'], $this->resolveFormOption($option, $formOption));
        }

        $builder->add('save', 'submit');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_setting_form';
    }

    private function resolveFormOption($option, &$formOption)
    {
        if ($option['type'] == 'choice') {
            $formOption['choices'] = $option['choices'];
        }

        return $formOption;
    }
}
