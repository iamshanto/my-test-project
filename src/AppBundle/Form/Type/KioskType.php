<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class KioskType extends AbstractType
{
    private $backOffices;
    private $users;

    public function __construct($backOffices, $users)
    {
        $this->backOffices = $backOffices;
        $this->users = $users;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kioskCode', null, array(
                'constraints' => array(
                    new NotBlank()
                )
            ))
            ->add('kioskName', null, array(
                'constraints' => array(
                    new NotBlank()
                )
            ))->add('address', 'textarea')
            ->add('insuranceAmount')
            ->add('bankName', null, array(
                'constraints' => array(
                    new NotBlank()
                )
            ))
            ->add('branchName', null, array(
                'constraints' => array(
                    new NotBlank()
                )
            ))
            //->add('heartBitDateTime')
            ->add('backOfficeId', 'choice', array(
                'choices' => $this->backOffices,
                'label' => 'Back Office'
            ))
            ->add('save', 'submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Kiosk'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_kiosk';
    }
}
