<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class BinInfoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('binCode', null, array(
                'constraints' => array(
                    new NotBlank()
                )
            ))
            ->add('typeName', 'choice', array(
                'choices' => array(
                    'Account' => 'Account',
                    'CreditCard' => 'Credit Card',
                    'DebitCard' => 'Debit Card',
                ),
                'constraints' => array(
                    new NotBlank()
                )
            ))->add('save', 'submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\BinInfo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_bininfo';
    }
}
