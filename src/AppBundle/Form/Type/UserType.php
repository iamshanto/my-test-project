<?php
namespace AppBundle\Form\Type;

use Emicro\UserBundle\Permission\Provider\SecurityPermissionProvider;
use Emicro\UserBundle\Validator\Constraints\Alpha;
use Emicro\UserBundle\Validator\Constraints\Alphanumeric;
use Emicro\UserBundle\Validator\Constraints\CustodianPassword;
use Emicro\UserBundle\Validator\Constraints\Digit;
use Emicro\UserBundle\Validator\Constraints\IsNewPassword;
use Emicro\UserBundle\Validator\Constraints\LowerCase;
use Emicro\UserBundle\Validator\Constraints\UpperCase;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType {

    private $permissionManager;
    private $isNewUser;
    private $userGroups;

    /** @var Request */
    private $request;
    private $user;
    private $settings;

    public function __construct($isNewUser, $permissionManager, $userGroups, $request, $settings)
    {
        $this->isNewUser = $isNewUser;
        $this->permissionManager = $permissionManager;
        $this->userGroups = $userGroups;
        $this->request = $request;
        $this->settings = $settings;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user = $this->isNewUser ? null : $options['data'];

        $builder->add('name', null, array(
            'constraints' => array(
                new NotBlank()
            )
        ) );

        $passwordConstraints = array();
        if ($this->isNewUser) {
            $passwordConstraints[] = new NotBlank();

            $builder->add('username', null, array(
                'constraints' => array(
                    new NotBlank()
                ),
                'label' => 'Employee ID'
            ) );
            $builder->add( 'email', 'email', array(
                'constraints' => array(
                    new NotBlank(),
                    new Email()
                )
            ) );
        }

        $passwordConstraints = array_merge($passwordConstraints, $this->getPasswordConstrains());
        $builder->add( 'plain_password', 'repeated', array(
            'constraints' => $passwordConstraints,
            'type'            => 'password',
            'invalid_message' => 'The password fields must match.',
            'options'         => array( 'attr' => array( 'class' => 'password-field' ) ),
            'required'        => true,
            'first_options'   => array( 'label' => 'Password' ),
            'second_options'  => array( 'label' => 'Repeat Password' ),
        ) );

        $builder->add( 'groupId', 'choice', array(
            'choices' => $this->userGroups,
            'label' => 'Group Name'
        ) );

        $builder->add( 'roles', 'choice', array(
            'choices' => $this->permissionManager->getPermissionHierarchyForChoiceField(),
            'multiple' => true,
            'attr' => array(
                'class' => 'multi-select'
            )
        ) );

        $builder->add( 'save', 'submit' );
    }

    public function getName()
    {
        return 'userType';
    }

    private function getPasswordConstrains()
    {
        $minLength = (isset($this->settings['min_password_length'])) ? (int)$this->settings['min_password_length'] : 6;
        $constrains = array();

        $formData = $this->request->request->all();

        if ($this->isNewUser || !empty($formData['userType']['plain_password']['first'])) {
            $constrains[] = new Length(array(
                'min' => $minLength
            ));
            $constrains[] = new IsNewPassword($this->user);
            $constrains[] = new Digit();
        }


        if (!empty($formData['userType']['plain_password']['first'])) {
            if (isset($formData['userType']['roles']) && in_array(SecurityPermissionProvider::ROLE_CUSTODIAN, $formData['userType']['roles'])) {
                $constrains[] = new CustodianPassword();
            } else {
                $constrains[] = new UpperCase();
                $constrains[] = new LowerCase();
            }
        }

        return $constrains;
    }
}