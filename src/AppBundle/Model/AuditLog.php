<?php

namespace AppBundle\Model;


class AuditLog extends BaseReport
{

    public function getLogByDateAndUser($start, $end, $username = null)
    {
        if ($end < $start) {
            return array();
        }

        $start = (new \DateTime($start))->format('Y-m-d H:i:s');
        $end = (new \DateTime($end))->format('Y-m-d H:i:s');

        return $this->getRecordsForReportByCriteria('LOG_BY_CRITERIA', array($start, $end, $username));
    }

    public function getKioskAuditLog($start, $end, $kiosk = null, $filterBy = '', $filterValue = '')
    {
        $start = (new \DateTime($start))->format('Y-m-d H:i:s');
        $end = (new \DateTime($end))->format('Y-m-d H:i:s');
        $message = '%%';
        if (!empty($filterBy)) {
            $filterOption = $this->getKioskFilterOptions();
            $message = sprintf($filterOption[$filterBy], $filterValue);
        }

        return $this->getRecordsForReportByCriteria('KIOSK', array($start, $end, $kiosk, $message));
    }

    public function getKioskFilterOptions()
    {
        return array(
            'Env_No' => 'Env No : %s',
            'Account_No' => 'Account No : %s',
            'Card_No' => 'last 4 Digit Card No : %s',
            'Customer_ID' => 'custId = %s',
            'Transaction_ID' => 'Transaction ID = %s',
        );
    }

    public function getPackageName()
    {
        return 'REPORT_AUDIT_LOG';
    }
}