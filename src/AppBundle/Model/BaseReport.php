<?php

namespace AppBundle\Model;

use AppBundle\Traits\OracleConnection;
use Doctrine\ORM\EntityManager;

abstract class BaseReport
{
    use OracleConnection;

    const REPORT_SP_TEMPLATE = '%s.%s(:results %s)';

    /**
     * @var EntityManager
     */
    private $_em;

    public function __construct(EntityManager $entityManager)
    {
        $this->_em = $entityManager;
    }

    public function getRecordsForReportByCriteria($reportName, $criteria = array())
    {

        $entityCollection = array();

        try {
            $connection = $this->getConnectionResource();

            $sql = $this->buildQueryStr($reportName, $criteria);

            $stmt = $this->prepareStoredProcedureStatement($sql);

            $results = oci_new_cursor($connection);
            oci_bind_by_name($stmt, ":results", $results, -1, OCI_B_CURSOR);

            foreach ($criteria as $field => $value) {
                oci_bind_by_name($stmt, ":field_{$field}", $criteria[$field]);
            }

            oci_execute($stmt);
            oci_execute($results);

            while (($row = oci_fetch_array($results, OCI_ASSOC + OCI_RETURN_NULLS)) != false) {
                $entityCollection[] = $row;
            }

        } catch (\Exception $e) {
            return array();
        }

        return $entityCollection;
    }


    protected function buildQueryStr($reportName, $criteria = array())
    {
        if (empty($criteria)) {
            $params = "";
        } else {
            $params = ", :" . implode(", :", array_map(function ($val) {
                    return 'field_' . $val;
                }, array_keys($criteria)));
        }

        return sprintf(self::REPORT_SP_TEMPLATE, $this->getPackageName(), $reportName, $params);
    }

    /**
     * @return EntityManager
     * */
    public function getEntityManager()
    {
        return $this->_em;
    }

    abstract public function getPackageName();
}