<?php

namespace AppBundle\Model;


class Transaction extends BaseReport
{
    public function getLiveTransactionReport()
    {
        $results = $this->getRecordsForReportByCriteria('LIVE');

        return $this->prepareLiveTransactionDataForView($results);
    }

    public function getSessionWiseReport($session)
    {
        $results = $this->getRecordsForReportByCriteria('SESSION_WISE', array($session));

        return $this->prepareSessionTransactionDataForView($results);
    }

    public function getDailyTransactions($kiosk, $start, $end)
    {
        $start = (new \DateTime($start))->format('Y-m-d H:i:s');
        $end = (new \DateTime($end))->format('Y-m-d H:i:s');

        $allData = $this->getRecordsForReportByCriteria('KIOSK_WISE_DAILY', array($kiosk, $start, $end));
        $formattedData = $this->prepareDailyTransactionDataForView($allData);
        return $formattedData;
    }

    public function getMonthlyTransactions($kiosk, $start, $end)
    {
        $start = (new \DateTime($start))->format('Y-m-d 00:00:00');
        $end = (new \DateTime($end))->format('Y-m-d 00:00:00');

        $allData = $this->getRecordsForReportByCriteria('KIOSK_WISE_MONTHLY', array($kiosk, $start, $end));

        $formattedData = $this->prepareMonthlyTransactionDataForView($allData);
        return $formattedData;
    }

    public function getYearlyTransactions($kiosk, $start, $end)
    {
        $start = $start . "01-01";
        $end = $end . "-12-31";

        $allData = $this->getRecordsForReportByCriteria('KIOSK_WISE_MONTHLY', array($kiosk, $start, $end));

        $formattedData = $this->prepareYearlyTransactionDataForView($allData);
        return $formattedData;
    }

    public function getPackageName()
    {
        return 'REPORT_TRANSACTION';
    }

    /**
     * @param $results
     * @return array
     */
    protected function prepareSessionTransactionDataForView($results)
    {
        $output = array();

        $grandTotal = 0;
        foreach ($results as $row) {
            $typeKey = $this->getPaymentTypeByPaymentMode($row['REFERENCETYPEID']);
            $modeKey = $this->getPaymentModeById($row['REFERENCETYPEID']);
            $amount = (float)$row['AMOUNT'];
            $output[$typeKey][$modeKey]['total'] = (
                isset($output[$typeKey][$modeKey]['total']) ? $output[$typeKey][$modeKey]['total'] + $amount : $amount
            );

            $output[$typeKey][$modeKey]['rows'][] = $row;
            $grandTotal += $amount;
        }

        return array(
            'reportData' => $output,
            'reportGrandTotal' => $grandTotal,
            'sessionStartEnd' => $this->getSessionStartEndDate($results),
        );
    }

    protected function getPaymentTypeByPaymentMode($modeId)
    {
        $paymentType = '';
        switch ($modeId) {
            case '1':
            case '3':
                $paymentType = 'Account Deposit';
                break;
            case '4':
                $paymentType = 'Credit Card Bill';
                break;
        }

        return $paymentType;
    }

    protected function getPaymentModeById($modeId)
    {
        $modeName = '';
        switch ($modeId) {
            case '1': $modeName = 'CashDeposit'; break;
            case '3': $modeName = 'CheckDeposit'; break;
            case '4': $modeName = 'CreditCardDeposit'; break;
        }

        return $modeName;
    }

    protected function prepareDailyTransactionDataForView($results)
    {
        $output = array();
        $emptyValue = array('CashDeposit' => 0, 'CheckDeposit' => 0, 'CreditCardDeposit' => 0, 'Total' => 0);
        $grandTotal = $emptyValue;
        foreach ($results as $row) {

            $modeKey = $this->getPaymentModeById($row['REFERENCETYPEID']);
            $amount = (float)$row['AMOUNT'];
            $key = (new \DateTime($row['TRANSACTION_DATE']))->format('d M Y');
            if (!array_key_exists($key, $output)) {
                $output[$key] = $emptyValue;
            }

            $output[$key][$modeKey] += $amount;
            $output[$key]['Total'] += $amount;

            $grandTotal[$modeKey] += $amount;
            $grandTotal['Total'] += $amount;
        }

        return array(
            'reportData' => $output,
            'reportGrandTotal' => $grandTotal,
            'chartData' => $this->prepareChartData($output, 'Total')
        );
    }

    protected function prepareMonthlyTransactionDataForView($results)
    {
        $output = array();
        $emptyValue = array('CashDeposit' => 0, 'CheckDeposit' => 0, 'CreditCardDeposit' => 0, 'Total' => 0);
        $grandTotal = $emptyValue;
        foreach ($results as $row) {
            $monthYear = (new \DateTime($row['TRANSACTION_YEAR'] . '-' .$row['TRANSACTION_MONTH'] . '-1'))->format('M y');

            $modeKey = $this->getPaymentModeById($row['REFERENCETYPEID']);
            $amount = (float)$row['AMOUNT'];

            if (!array_key_exists($monthYear, $output)) {
                $output[$monthYear] = $emptyValue;
            }

            $output[$monthYear][$modeKey] += $amount;
            $output[$monthYear]['Total'] += $amount;

            $grandTotal[$modeKey] += $amount;
            $grandTotal['Total'] += $amount;
        }

        return array(
            'reportData' => $output,
            'reportGrandTotal' => $grandTotal,
            'chartData' => $this->prepareChartData($output, 'Total')
        );
    }

    protected function prepareYearlyTransactionDataForView($results)
    {
        $output = array();
        $emptyValue = array('CashDeposit' => 0, 'CheckDeposit' => 0, 'CreditCardDeposit' => 0, 'Total' => 0);
        $grandTotal = $emptyValue;
        foreach ($results as $row) {

            $year = $row['TRANSACTION_YEAR'];
            $modeKey = $this->getPaymentModeById($row['REFERENCETYPEID']);
            $amount = (float)$row['AMOUNT'];

            if (!array_key_exists($year, $output)) {
                $output[$year] = $emptyValue;
            }

            $output[$year][$modeKey] += $amount;
            $output[$year]['Total'] += $amount;

            $grandTotal[$modeKey] += $amount;
            $grandTotal['Total'] += $amount;
        }

        return array(
            'reportData' => $output,
            'reportGrandTotal' => $grandTotal,
            'chartData' => $this->prepareChartData($output, 'Total')
        );
    }

    protected function prepareLiveTransactionDataForView($results)
    {
        $output = array();
        $grandTotal = 0;

        foreach ($results as $row) {
            $data = array();
            $data['DATE'] = (new \DateTime($row['TRANSACTIONDATETIME']))->format('d M y, h:i A');
            $data['TYPE'] = $this->getPaymentModeById($row['REFERENCETYPEID']);
            $data['KIOSK'] = $row['KIOSKID'];
            $data['AMOUNT'] = $row['AMOUNT'];
            $output[] = $data;

            $grandTotal += (float)$row['AMOUNT'];
        }

        return array(
            'reportData' => $output,
            'reportGrandTotal' => $grandTotal
        );
    }

    protected function prepareChartData($results, $field)
    {
        $chartData = array();
        foreach ($results as $key=> $row) {
            $chartData[] = array($key, $row[$field]);
        }

        return $chartData;
    }

    protected function getSessionStartEndDate($results)
    {
        if (empty($results)) {
            return array();
        }

        $end = end($results);
        $output = array(
            'startDate' => $results[0]['TRANSACTIONDATETIME'],
            'endDate' => $end['TRANSACTIONDATETIME'],
        );
        return $output;
    }
}