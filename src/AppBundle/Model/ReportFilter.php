<?php

namespace AppBundle\Model;


class ReportFilter extends BaseReport
{
    public function getSessionsByKioskAndDate($kiosk, $date)
    {
        return $this->getRecordsForReportByCriteria('KIOSK_SESSION', array($kiosk, $date));
    }

    public function getAuditLogUsers()
    {
        return $this->getRecordsForReportByCriteria('AUDIT_LOG_USERS', array());
    }

    public function getPackageName()
    {
        return 'REPORT_FILTER';
    }
}