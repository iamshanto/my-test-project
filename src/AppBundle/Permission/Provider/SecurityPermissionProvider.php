<?php

namespace AppBundle\Permission\Provider;
use Emicro\UserBundle\Permission\Provider\ProviderInterface;

/**
 * Security Provider
 *
 * @author Mohammad Emran Hasan <phpfour@gmail.com>
 */
class SecurityPermissionProvider implements ProviderInterface
{
    public function getPermissions()
    {
        return array(
            'SYSTEM_MANAGE' =>
                array(
                    'ROLE_KIOSK_LIST', 'ROLE_KIOSK_ADD', 'ROLE_KIOSK_EDIT', 'ROLE_KIOSK_DELETE',
                    'ROLE_BININFO_LIST', 'ROLE_BININFO_ADD', 'ROLE_BININFO_EDIT', 'ROLE_BININFO_DELETE',
                    'ROLE_BACK_OFFICE_LIST', 'ROLE_BACK_OFFICE_ADD', 'ROLE_BACK_OFFICE_EDIT', 'ROLE_BACK_OFFICE_DELETE',
                    'ROLE_SETTING_EDIT', 'ROLE_ACCESS_ALL_KIOSK', 'ROLE_KIOSK_STATUS'
                ),
            'REPORT' => array(
                'ROLE_REPORT_SESSION',
                'ROLE_REPORT_DAILY',
                'ROLE_REPORT_MONTHLY',
                'ROLE_REPORT_YEARLY',
                'ROLE_REPORT_LIVE',
                'ROLE_REPORT_DATE_TO_DATE',
                'ROLE_REPORT_AUDIT',
                'ROLE_REPORT_KIOSK_AUDIT',
            ),
        );
    }
}