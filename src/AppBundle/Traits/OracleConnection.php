<?php

namespace AppBundle\Traits;

use Doctrine\ORM\EntityManager;

trait OracleConnection
{
    /**
     * @return resource
     */
    protected function getConnectionResource()
    {
        return $this->getEntityManager()->getConnection()->getWrappedConnection()->getConnectionResource();
    }

    protected function prepareStoredProcedureStatement($sql)
    {
        return oci_parse($this->getConnectionResource(), 'BEGIN ' . $sql . '; END;');
    }

    /**
     * @return EntityManager
     * */
    abstract public function getEntityManager();
}