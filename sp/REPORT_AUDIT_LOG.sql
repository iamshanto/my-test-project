CREATE OR REPLACE PACKAGE REPORT_AUDIT_LOG AS

  PROCEDURE LOG_BY_CRITERIA(
    log_cursor_out OUT SYS_REFCURSOR,
    start_date     IN DATE,
    end_date       IN DATE,
    username       IN VARCHAR
  );

  PROCEDURE KIOSK(
    kiosk_log_cursor_out OUT SYS_REFCURSOR,
    start_date           IN DATE,
    end_date             IN DATE,
    kiosk                IN VARCHAR,
    msg                  IN VARCHAR
  );

END REPORT_AUDIT_LOG;


CREATE OR REPLACE PACKAGE BODY REPORT_AUDIT_LOG AS

  /**
   * Audit log by search criteria
   */
  PROCEDURE LOG_BY_CRITERIA(
    log_cursor_out OUT SYS_REFCURSOR,
    start_date     IN DATE,
    end_date       IN DATE,
    username       IN VARCHAR
  ) AS

    BEGIN
      IF username IS NULL OR username = ''
      THEN
        OPEN log_cursor_out FOR
        SELECT
          TYPE_ID,
          TYPE_NAME,
          DESCRIPTION,
          EVENT_TIME,
          USER_NAME,
          IP_ADDRESS
        FROM APP_AUDIT_LOG
        WHERE TRUNC(APP_AUDIT_LOG.EVENT_TIME) BETWEEN TRUNC(start_date) AND TRUNC(end_date)
        ORDER BY APP_AUDIT_LOG.EVENT_TIME ASC;
      ELSE
        OPEN log_cursor_out FOR
        SELECT
          TYPE_ID,
          TYPE_NAME,
          DESCRIPTION,
          EVENT_TIME,
          USER_NAME,
          IP_ADDRESS
        FROM APP_AUDIT_LOG
        WHERE
          APP_AUDIT_LOG.USER_NAME = username
          AND TRUNC(APP_AUDIT_LOG.EVENT_TIME) BETWEEN TRUNC(start_date) AND TRUNC(end_date)
        ORDER BY APP_AUDIT_LOG.EVENT_TIME ASC;
      END IF;

    END LOG_BY_CRITERIA;

  /**
   * Audit log by search criteria
   */
  PROCEDURE KIOSK(
    kiosk_log_cursor_out OUT SYS_REFCURSOR,
    start_date           IN DATE,
    end_date             IN DATE,
    kiosk                IN VARCHAR,
    msg                  IN VARCHAR
  ) AS

    BEGIN
      IF kiosk IS NULL OR kiosk = ''
      THEN
        OPEN kiosk_log_cursor_out FOR
        SELECT *
        FROM KIOSKAUDITTRAI
        WHERE
          TRUNC(AUDITTRAILDATETIME) BETWEEN TRUNC(start_date) AND TRUNC(end_date)
          AND LOWER(KIOSKAUDITTRAI.KIOSKMESSAGE) LIKE lower(msg)
        ORDER BY AUDITTRAILDATETIME ASC;
      ELSE
        OPEN kiosk_log_cursor_out FOR
        SELECT *
        FROM KIOSKAUDITTRAI
        WHERE
          TRUNC(AUDITTRAILDATETIME) BETWEEN TRUNC(start_date) AND TRUNC(end_date)
          AND KIOSKAUDITTRAI.KIOSKCODE = kiosk
          AND LOWER(KIOSKAUDITTRAI.KIOSKMESSAGE) LIKE lower(msg)
        ORDER BY AUDITTRAILDATETIME ASC;
      END IF;
    END KIOSK;
END REPORT_AUDIT_LOG;
