CREATE OR REPLACE PACKAGE REPORT_FILTER AS

  PROCEDURE KIOSK_SESSION(
    session_cursor_out OUT SYS_REFCURSOR,
    kiosk_code         IN VARCHAR2,
    session_date       IN DATE
  );

  PROCEDURE AUDIT_LOG_USERS(
    audit_user_cursor_out OUT SYS_REFCURSOR
  );

END REPORT_FILTER;

CREATE OR REPLACE PACKAGE BODY REPORT_FILTER AS

  /**
  * Session list of a day by kiosk
  */
  PROCEDURE KIOSK_SESSION(
    session_cursor_out OUT SYS_REFCURSOR,
    kiosk_code         IN VARCHAR2,
    session_date       IN DATE
  ) AS

    BEGIN
      OPEN session_cursor_out FOR
      SELECT FILENAME AS SESSIONID
      FROM FILECOPYINFO
      WHERE
        FILECOPYINFO.CREATEDBY = kiosk_code
        AND TRUNC(FILECOPYINFO.CREATEDDATE) = TRUNC(session_date);

    END KIOSK_SESSION;

  /**
  * List of all user with audit log entry
  */
  PROCEDURE AUDIT_LOG_USERS(
    audit_user_cursor_out OUT SYS_REFCURSOR
  ) AS

    BEGIN
      OPEN audit_user_cursor_out FOR
      SELECT DISTINCT USER_NAME
      FROM APP_AUDIT_LOG;

    END AUDIT_LOG_USERS;

END REPORT_FILTER;
