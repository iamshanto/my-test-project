CREATE OR REPLACE PACKAGE REPORT_TRANSACTION AS

  PROCEDURE YEARLY_AMOUNT(
    entries_cursor_out OUT SYS_REFCURSOR,
    start_year         IN INTEGER,
    end_year           IN INTEGER
  );

  PROCEDURE MONTHLY_AMOUNT(
    monthly_cursor_out OUT SYS_REFCURSOR,
    start_date        IN DATE,
    end_date          IN DATE
  );

  PROCEDURE SESSION_WISE(
    session_cursor_out OUT SYS_REFCURSOR,
    session_id         IN VARCHAR2
  );

  PROCEDURE LIVE(
    live_cursor_out OUT SYS_REFCURSOR,
    kiosk         IN VARCHAR2 DEFAULT NULL
  );

  PROCEDURE KIOSK_WISE_DAILY(
    kiosk_wise_cursor_out OUT SYS_REFCURSOR,
    kiosk                 IN VARCHAR2,
    start_date            IN DATE,
    end_date              IN DATE
  );

  PROCEDURE KIOSK_WISE_MONTHLY(
    kiosk_monlhly_cursor_out OUT SYS_REFCURSOR,
    kiosk                    IN VARCHAR2,
    start_date              IN DATE,
    end_date                IN DATE
  );

END REPORT_TRANSACTION;

CREATE OR REPLACE PACKAGE BODY REPORT_TRANSACTION AS

  /**
  * Yearly amount report
  */
  PROCEDURE YEARLY_AMOUNT(
    entries_cursor_out OUT SYS_REFCURSOR,
    start_year         IN INTEGER,
    end_year           IN INTEGER
  ) AS

    BEGIN
      OPEN entries_cursor_out FOR
      SELECT
        EXTRACT(YEAR FROM TRANSACTIONDATETIME) transaction_year,
        COUNT(TRANSACTIONMASTERID)             transaction_count,
        SUM(ALL AMOUNT)                        AMOUNT
      FROM TRANSACTIONMASTER
      WHERE
        EXTRACT(YEAR FROM TRANSACTIONDATETIME)
        BETWEEN start_year AND end_year
      GROUP BY EXTRACT(YEAR FROM TRANSACTIONDATETIME);

    END YEARLY_AMOUNT;


  /** 
  * Monthly amount report 
  */
  PROCEDURE MONTHLY_AMOUNT(
    monthly_cursor_out OUT SYS_REFCURSOR,
    start_date        IN DATE,
    end_date          IN DATE
  ) AS

    BEGIN
      OPEN monthly_cursor_out FOR
      SELECT
        EXTRACT(YEAR FROM TRANSACTIONDATETIME)  transaction_year,
        EXTRACT(MONTH FROM TRANSACTIONDATETIME) transaction_month,
        COUNT(TRANSACTIONMASTERID)              transaction_count,
        SUM(ALL AMOUNT)                         deposit_amount
      FROM TRANSACTIONMASTER
      WHERE
        TRUNC(TRANSACTIONMASTER.TRANSACTIONDATETIME)
        BETWEEN TRUNC(start_date) AND TRUNC(end_date)
      GROUP BY EXTRACT(YEAR FROM TRANSACTIONDATETIME), EXTRACT(MONTH FROM TRANSACTIONDATETIME);

    END MONTHLY_AMOUNT;

  /** 
  * Session wise report for a day 
  */
  PROCEDURE SESSION_WISE(
    session_cursor_out OUT SYS_REFCURSOR,
    session_id         IN VARCHAR2
  ) AS

    BEGIN
      OPEN session_cursor_out FOR
      SELECT
        SERVICETRANSACTIONID,
        TRANSACTIONDATETIME,
        USERACCOUNTNO,
        AMOUNT,
        REFERENCETYPEID
      FROM TRANSACTIONMASTER
      WHERE TRANSACTIONMASTER.SESSIONID = session_id;

    END SESSION_WISE;

  /**
  * Live transactions
  */
  PROCEDURE LIVE(
    live_cursor_out OUT SYS_REFCURSOR,
    kiosk         IN VARCHAR2 DEFAULT NULL
  ) AS

    BEGIN
      IF kiosk IS NULL THEN
        OPEN live_cursor_out FOR
        SELECT
          SERVICETRANSACTIONID,
          TRANSACTIONDATETIME,
          USERACCOUNTNO,
          AMOUNT,
          KIOSKID,
          REFERENCETYPEID
        FROM TRANSACTIONMASTER
        WHERE TRANSACTIONMASTER.SESSIONID IS NULL;
      ELSE
        OPEN live_cursor_out FOR
        SELECT
          SERVICETRANSACTIONID,
          TRANSACTIONDATETIME,
          USERACCOUNTNO,
          AMOUNT,
          KIOSKID,
          REFERENCETYPEID
        FROM TRANSACTIONMASTER
        WHERE TRANSACTIONMASTER.SESSIONID IS NULL AND KIOSKID = kiosk;
      END IF;

    END LIVE;

  /** 
  * Kiosk wise daily report 
  */
  PROCEDURE KIOSK_WISE_DAILY(
    kiosk_wise_cursor_out OUT SYS_REFCURSOR,
    kiosk                 IN VARCHAR2,
    start_date            IN DATE,
    end_date              IN DATE
  ) AS

    BEGIN
      OPEN kiosk_wise_cursor_out FOR
      SELECT
        TRUNC(TRANSACTIONMASTER.TRANSACTIONDATETIME) TRANSACTION_DATE,
        SUM(ALL AMOUNT)                              AMOUNT,
        REFERENCETYPEID
      FROM TRANSACTIONMASTER
      WHERE
        TRANSACTIONMASTER.KIOSKID = kiosk
        AND TRUNC(TRANSACTIONMASTER.TRANSACTIONDATETIME)
        BETWEEN TRUNC(start_date) AND TRUNC(end_date)
      GROUP BY TRUNC(TRANSACTIONMASTER.TRANSACTIONDATETIME), REFERENCETYPEID
      ORDER BY TRUNC(TRANSACTIONMASTER.TRANSACTIONDATETIME) ASC;

    END KIOSK_WISE_DAILY;

  /** 
  * Kiosk wise monthly report 
  */
  PROCEDURE KIOSK_WISE_MONTHLY(
    kiosk_monlhly_cursor_out OUT SYS_REFCURSOR,
    kiosk                    IN VARCHAR2,
    start_date              IN DATE,
    end_date                IN DATE
  ) AS

    BEGIN
      OPEN kiosk_monlhly_cursor_out FOR
      SELECT
        EXTRACT(YEAR FROM TRANSACTIONDATETIME)  transaction_year,
        EXTRACT(MONTH FROM TRANSACTIONDATETIME) transaction_month,
        SUM(ALL AMOUNT)                         AMOUNT,
        REFERENCETYPEID
      FROM TRANSACTIONMASTER
      WHERE
        (TRUNC(TRANSACTIONMASTER.TRANSACTIONDATETIME, 'MONTH')
        BETWEEN TRUNC(start_date, 'MONTH') AND TRUNC(end_date, 'MONTH'))
        AND TRANSACTIONMASTER.KIOSKID = kiosk

      GROUP BY EXTRACT(YEAR FROM TRANSACTIONDATETIME), EXTRACT(MONTH FROM TRANSACTIONDATETIME), REFERENCETYPEID
      ORDER BY EXTRACT(YEAR FROM TRANSACTIONDATETIME) ASC, EXTRACT(MONTH FROM TRANSACTIONDATETIME) ASC;

    END KIOSK_WISE_MONTHLY;

  /** 
  * Kiosk wise monthly report 
  */
  PROCEDURE KIOSK_WISE_YEARLY(
    kiosk_yearly_cursor_out OUT SYS_REFCURSOR,
    kiosk                   IN VARCHAR2,
    start_year              IN INTEGER,
    end_year                IN INTEGER
  ) AS

    BEGIN
      OPEN kiosk_yearly_cursor_out FOR
      SELECT
        EXTRACT(YEAR FROM TRANSACTIONDATETIME) transaction_year,
        SUM(ALL AMOUNT)                        AMOUNT,
        REFERENCETYPEID
      FROM TRANSACTIONMASTER
      WHERE
        (EXTRACT(YEAR FROM TRANSACTIONDATETIME)
        BETWEEN start_year AND end_year)
        AND TRANSACTIONMASTER.KIOSKID = kiosk

      GROUP BY EXTRACT(YEAR FROM TRANSACTIONDATETIME), REFERENCETYPEID
      ORDER BY EXTRACT(YEAR FROM TRANSACTIONDATETIME) ASC;

    END KIOSK_WISE_YEARLY;

END REPORT_TRANSACTION;